{
    "id": "4ce204b5-b930-4a2f-b119-12999c6c1eff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_parque",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cda4716a-0cea-47d1-bee0-6b7c033cc253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ce204b5-b930-4a2f-b119-12999c6c1eff",
            "compositeImage": {
                "id": "435ca1fc-35ff-46d4-abfc-51ef5164b08e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda4716a-0cea-47d1-bee0-6b7c033cc253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0628396-318e-4a2b-9e42-90097fb9d041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda4716a-0cea-47d1-bee0-6b7c033cc253",
                    "LayerId": "8f80aa1e-396f-42e7-a6d6-69498a355417"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "8f80aa1e-396f-42e7-a6d6-69498a355417",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ce204b5-b930-4a2f-b119-12999c6c1eff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}