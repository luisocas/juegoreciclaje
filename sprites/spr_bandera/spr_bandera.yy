{
    "id": "6d8e7473-6a80-4414-af89-020b5e5eb4e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bandera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 4,
    "bbox_right": 254,
    "bbox_top": 68,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38ccb5fb-6d7a-4baf-b6ac-dfcdaa796e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8e7473-6a80-4414-af89-020b5e5eb4e2",
            "compositeImage": {
                "id": "f78d0e83-c1bd-4d7d-9a67-95454282d5d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ccb5fb-6d7a-4baf-b6ac-dfcdaa796e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4acc157-0c27-4774-9955-5154f632e949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ccb5fb-6d7a-4baf-b6ac-dfcdaa796e31",
                    "LayerId": "3f18b640-d0ee-48ce-baeb-ab9695593d10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "3f18b640-d0ee-48ce-baeb-ab9695593d10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d8e7473-6a80-4414-af89-020b5e5eb4e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}