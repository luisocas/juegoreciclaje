{
    "id": "7b687198-04e6-45b2-84e8-c78b8b388559",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_jump_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 14,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cd8a046-4590-4e7e-a75d-425f9ae7cd1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b687198-04e6-45b2-84e8-c78b8b388559",
            "compositeImage": {
                "id": "3c7d0586-4fb5-456f-a31c-ac0ab4d44c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cd8a046-4590-4e7e-a75d-425f9ae7cd1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08bffb8-076a-4eaf-8f8b-33d93e0a7fc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cd8a046-4590-4e7e-a75d-425f9ae7cd1d",
                    "LayerId": "469ac737-5ceb-4060-8dd1-161098df28e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "469ac737-5ceb-4060-8dd1-161098df28e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b687198-04e6-45b2-84e8-c78b8b388559",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 112
}