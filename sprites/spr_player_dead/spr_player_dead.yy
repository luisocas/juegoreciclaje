{
    "id": "10e610cb-badc-4dd8-8952-f6fd14f156af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 7,
    "bbox_right": 217,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "641c43fa-96df-44a3-a0fb-c0d8c8cd2567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e610cb-badc-4dd8-8952-f6fd14f156af",
            "compositeImage": {
                "id": "c17a0e33-f68f-4bd0-b57f-ef03f2a6d7d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "641c43fa-96df-44a3-a0fb-c0d8c8cd2567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6297c771-0f29-4cb7-b053-7832a36951fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "641c43fa-96df-44a3-a0fb-c0d8c8cd2567",
                    "LayerId": "ca123478-dda9-4ff8-b2c5-e0a4c002c3ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "ca123478-dda9-4ff8-b2c5-e0a4c002c3ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e610cb-badc-4dd8-8952-f6fd14f156af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}