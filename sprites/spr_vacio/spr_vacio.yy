{
    "id": "be5f8e8a-49fb-454d-9502-efd68c4a7800",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vacio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a9a39b6-28c9-46d8-aa53-5778aa9bfefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5f8e8a-49fb-454d-9502-efd68c4a7800",
            "compositeImage": {
                "id": "1edbe9a0-b80e-476b-a2a1-fb5d871eaefc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9a39b6-28c9-46d8-aa53-5778aa9bfefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72ebd3e-dcd9-40da-be56-b8a5fb1d756a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9a39b6-28c9-46d8-aa53-5778aa9bfefd",
                    "LayerId": "6f641b5d-c063-4a0c-b770-f8660fe375d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f641b5d-c063-4a0c-b770-f8660fe375d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be5f8e8a-49fb-454d-9502-efd68c4a7800",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}