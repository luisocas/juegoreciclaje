{
    "id": "008755fd-411f-441a-b8d3-ba4606b252a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_txt_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 9,
    "bbox_right": 759,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41f03c3b-966b-4178-b75c-e36826f39cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008755fd-411f-441a-b8d3-ba4606b252a1",
            "compositeImage": {
                "id": "d3406f73-124a-4568-81bb-a5b827b71f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f03c3b-966b-4178-b75c-e36826f39cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d561d4-7df8-441f-b8e0-16541b7b92c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f03c3b-966b-4178-b75c-e36826f39cba",
                    "LayerId": "14c6c267-62d9-4e92-bf3e-1e184581e247"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 247,
    "layers": [
        {
            "id": "14c6c267-62d9-4e92-bf3e-1e184581e247",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "008755fd-411f-441a-b8d3-ba4606b252a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}