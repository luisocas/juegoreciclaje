{
    "id": "ad215019-2f32-4ad8-b114-51e223df5204",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e94341e8-6b13-43c5-be6d-4b345b8fd604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad215019-2f32-4ad8-b114-51e223df5204",
            "compositeImage": {
                "id": "bed0bbaa-725c-42b1-bca1-e00e68c4a722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e94341e8-6b13-43c5-be6d-4b345b8fd604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55fbe75d-45e7-4280-b917-443bef8ba9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e94341e8-6b13-43c5-be6d-4b345b8fd604",
                    "LayerId": "3f205ed8-be2d-4955-a8f9-0e6d1b81a68b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3f205ed8-be2d-4955-a8f9-0e6d1b81a68b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad215019-2f32-4ad8-b114-51e223df5204",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}