{
    "id": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vida",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96d49e94-998c-4f11-b4db-b0f6f6817684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
            "compositeImage": {
                "id": "c9559d84-324d-449b-974f-0d39fd73845c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d49e94-998c-4f11-b4db-b0f6f6817684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "913be5f1-1c09-4ea0-8a5d-51b0b26dfd58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d49e94-998c-4f11-b4db-b0f6f6817684",
                    "LayerId": "795888fe-939b-4f25-97d0-674d5804b053"
                }
            ]
        },
        {
            "id": "4f899dd1-29c7-4fbc-9cb3-6c61b2b6fe68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
            "compositeImage": {
                "id": "bb7cb78e-7b66-4fda-82d1-e0669e80b880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f899dd1-29c7-4fbc-9cb3-6c61b2b6fe68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01bfe6ab-8784-44f5-9246-e88ea14a7fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f899dd1-29c7-4fbc-9cb3-6c61b2b6fe68",
                    "LayerId": "795888fe-939b-4f25-97d0-674d5804b053"
                }
            ]
        },
        {
            "id": "738348a5-a27d-4d76-86a0-5e614b3ead3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
            "compositeImage": {
                "id": "d7af3d26-f3db-4042-8090-e7a1d54d9e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "738348a5-a27d-4d76-86a0-5e614b3ead3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa3d748-dc38-4a66-84e2-43e98530f4c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "738348a5-a27d-4d76-86a0-5e614b3ead3b",
                    "LayerId": "795888fe-939b-4f25-97d0-674d5804b053"
                }
            ]
        },
        {
            "id": "5634d25b-bb6e-443d-8b3a-efbd600f544a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
            "compositeImage": {
                "id": "98f69bb2-e1ef-4445-adfa-0a29e9925892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5634d25b-bb6e-443d-8b3a-efbd600f544a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564902e5-6b18-4fa0-b1dc-0e5035eda7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5634d25b-bb6e-443d-8b3a-efbd600f544a",
                    "LayerId": "795888fe-939b-4f25-97d0-674d5804b053"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "795888fe-939b-4f25-97d0-674d5804b053",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}