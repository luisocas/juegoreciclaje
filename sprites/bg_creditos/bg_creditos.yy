{
    "id": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_creditos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38637d10-0bbf-4eaa-a999-e220749d300f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
            "compositeImage": {
                "id": "ec57c28d-211d-42a8-bb60-eb4bb6a2af16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38637d10-0bbf-4eaa-a999-e220749d300f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3d19de-5661-47b9-add1-39b4fda93a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38637d10-0bbf-4eaa-a999-e220749d300f",
                    "LayerId": "12138ab0-c239-426f-bfc1-363f23a3ae53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "12138ab0-c239-426f-bfc1-363f23a3ae53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}