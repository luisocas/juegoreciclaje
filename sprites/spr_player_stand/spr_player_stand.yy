{
    "id": "ecbd44ff-b7d1-450b-9d84-fde8b93ed972",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 17,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16cbf60f-1416-4869-8648-1688954a0e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecbd44ff-b7d1-450b-9d84-fde8b93ed972",
            "compositeImage": {
                "id": "684f2767-581a-46cc-9277-48058e38fa6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cbf60f-1416-4869-8648-1688954a0e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05bc77b5-7e96-4658-abda-1395f7eff522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cbf60f-1416-4869-8648-1688954a0e53",
                    "LayerId": "3a2e43c1-4df6-4291-95e4-ddc44dc61956"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "3a2e43c1-4df6-4291-95e4-ddc44dc61956",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecbd44ff-b7d1-450b-9d84-fde8b93ed972",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 112
}