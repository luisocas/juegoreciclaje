{
    "id": "ac9dac6b-ea31-4913-9d15-182908f127ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a642df5e-db56-423f-a64c-f570dd7a1eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9dac6b-ea31-4913-9d15-182908f127ec",
            "compositeImage": {
                "id": "0fd91f83-9b6a-4972-b11e-2fc6595680d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a642df5e-db56-423f-a64c-f570dd7a1eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e55e43c-59f1-40f6-9584-4f073127d1c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a642df5e-db56-423f-a64c-f570dd7a1eb6",
                    "LayerId": "321f3c91-5ebb-4252-9eaa-c01a3dcf4849"
                }
            ]
        },
        {
            "id": "72411ca9-55a4-4b82-a38c-6fe831e8994f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9dac6b-ea31-4913-9d15-182908f127ec",
            "compositeImage": {
                "id": "0a7ac310-d094-4119-aebe-a3ac9d2595f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72411ca9-55a4-4b82-a38c-6fe831e8994f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a8dadf0-3e29-43da-b984-13fe18dba8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72411ca9-55a4-4b82-a38c-6fe831e8994f",
                    "LayerId": "321f3c91-5ebb-4252-9eaa-c01a3dcf4849"
                }
            ]
        },
        {
            "id": "a7205f07-53c2-4f62-bcc6-3c0e185a1c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9dac6b-ea31-4913-9d15-182908f127ec",
            "compositeImage": {
                "id": "5c0d4cc1-f86f-4520-890a-0ed23aad2a40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7205f07-53c2-4f62-bcc6-3c0e185a1c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9c34e2-5533-49d5-8348-dd15c6da5d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7205f07-53c2-4f62-bcc6-3c0e185a1c92",
                    "LayerId": "321f3c91-5ebb-4252-9eaa-c01a3dcf4849"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "321f3c91-5ebb-4252-9eaa-c01a3dcf4849",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac9dac6b-ea31-4913-9d15-182908f127ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 32
}