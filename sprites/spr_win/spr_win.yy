{
    "id": "b94f4567-1d73-4739-b046-f35335a71386",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_win",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 766,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f717791-38e3-4010-b57f-3c4848b03fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b94f4567-1d73-4739-b046-f35335a71386",
            "compositeImage": {
                "id": "ce32fd59-d1e6-4e47-99b3-35ccf24efe4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f717791-38e3-4010-b57f-3c4848b03fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979a8b7a-3508-467d-ba96-76cf65914bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f717791-38e3-4010-b57f-3c4848b03fc2",
                    "LayerId": "8aa2a3ac-7b47-46b2-b92b-fb45fbdf4600"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "8aa2a3ac-7b47-46b2-b92b-fb45fbdf4600",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b94f4567-1d73-4739-b046-f35335a71386",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}