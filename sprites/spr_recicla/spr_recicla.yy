{
    "id": "ad856191-8e0e-482f-a121-74b2aba7096a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recicla",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 1,
    "bbox_right": 93,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b549c45-305a-42bb-b417-10ab8d2de426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "compositeImage": {
                "id": "e37b7f33-f2a7-4562-8112-e96ffcead07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b549c45-305a-42bb-b417-10ab8d2de426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c900def4-72e3-4378-924e-58009b2bc8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b549c45-305a-42bb-b417-10ab8d2de426",
                    "LayerId": "3428b627-03ea-4647-94cd-e4c6f85986bb"
                }
            ]
        },
        {
            "id": "30bfde9b-29ad-4102-bd7c-7eab8a26abea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "compositeImage": {
                "id": "374e1cfa-9482-41b7-9c5a-aa84540c5182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30bfde9b-29ad-4102-bd7c-7eab8a26abea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b87a16-aca2-4b39-8d4c-103ff4c46aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30bfde9b-29ad-4102-bd7c-7eab8a26abea",
                    "LayerId": "3428b627-03ea-4647-94cd-e4c6f85986bb"
                }
            ]
        },
        {
            "id": "b65a5896-e433-442c-84a9-2f6a870c455b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "compositeImage": {
                "id": "0629c0d6-5650-49cb-8566-d421fdfe06dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65a5896-e433-442c-84a9-2f6a870c455b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12bd26f5-0aad-410a-bf39-884511aa6b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65a5896-e433-442c-84a9-2f6a870c455b",
                    "LayerId": "3428b627-03ea-4647-94cd-e4c6f85986bb"
                }
            ]
        },
        {
            "id": "e7fcb589-2e22-4aac-94fa-5b071fac6cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "compositeImage": {
                "id": "3bdeed5b-dfe2-4a6c-a152-0923104a1fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fcb589-2e22-4aac-94fa-5b071fac6cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30d4a18-edd3-4f1c-b1b9-b76a15781b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fcb589-2e22-4aac-94fa-5b071fac6cd8",
                    "LayerId": "3428b627-03ea-4647-94cd-e4c6f85986bb"
                }
            ]
        },
        {
            "id": "75ac8bb0-5749-4ef9-9845-93d6b3e54db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "compositeImage": {
                "id": "1e45d358-9f07-4f99-a6c6-827ada14bf16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ac8bb0-5749-4ef9-9845-93d6b3e54db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7372c6a6-f8ae-4d45-b904-54fa6649bbbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ac8bb0-5749-4ef9-9845-93d6b3e54db2",
                    "LayerId": "3428b627-03ea-4647-94cd-e4c6f85986bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3428b627-03ea-4647-94cd-e4c6f85986bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad856191-8e0e-482f-a121-74b2aba7096a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}