{
    "id": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 14,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1dff473-b59e-44fc-a177-09f5a2ccc939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "a02f0fae-ed56-49bd-bfba-b6d037c79a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1dff473-b59e-44fc-a177-09f5a2ccc939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d63126-a29a-4cda-80d5-aa67d69c1ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1dff473-b59e-44fc-a177-09f5a2ccc939",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        },
        {
            "id": "32cdea7d-44b9-4096-a9a0-108fdb67e689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "02fcf106-4f46-4bc7-9081-a68a651d0271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32cdea7d-44b9-4096-a9a0-108fdb67e689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b3dffd-111b-4bf1-b473-e015f66302cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32cdea7d-44b9-4096-a9a0-108fdb67e689",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        },
        {
            "id": "a4ddd9b6-26b5-49f4-b087-b7dac14b29b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "77a2ae26-f369-4871-9b09-6036a82b723f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ddd9b6-26b5-49f4-b087-b7dac14b29b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a02ad39-c21d-472e-8e66-28e523c74cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ddd9b6-26b5-49f4-b087-b7dac14b29b6",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        },
        {
            "id": "8659edd7-77a3-4402-b1e1-4b256b216da9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "b9c94308-d492-4da0-83bc-cf2f6f8eff00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8659edd7-77a3-4402-b1e1-4b256b216da9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f200ad4-0177-4dc6-9a6e-24fb8028a284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8659edd7-77a3-4402-b1e1-4b256b216da9",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        },
        {
            "id": "092ae192-90dc-499a-a9f3-caaf2d036f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "f8ded585-acf3-4f76-8b1d-1f51e52d95c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "092ae192-90dc-499a-a9f3-caaf2d036f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72ecc93-4277-475a-a9fb-cd97fa095c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "092ae192-90dc-499a-a9f3-caaf2d036f7c",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        },
        {
            "id": "42dbf094-d64d-461b-af78-2dd647f1e6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "compositeImage": {
                "id": "31c2b9ae-fc8f-4be2-b307-cbae34a6007e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42dbf094-d64d-461b-af78-2dd647f1e6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dbd4d1-5b50-4907-a2fd-ea26ac062d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42dbf094-d64d-461b-af78-2dd647f1e6e7",
                    "LayerId": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "31c7e3fe-a001-4c3e-8bc3-4b5249f8b900",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "768697b4-dfd3-4d70-8664-9cf04cb4a0f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 112
}