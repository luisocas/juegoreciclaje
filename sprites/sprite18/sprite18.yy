{
    "id": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98790643-005f-4225-a169-c27cc356fff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
            "compositeImage": {
                "id": "c1ce3166-e79f-4972-af8c-b5d6a5d2da62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98790643-005f-4225-a169-c27cc356fff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cfd4d1c-9cd7-4355-9c52-13237cf36a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98790643-005f-4225-a169-c27cc356fff0",
                    "LayerId": "15c73fa4-8187-4bf1-be48-7c46af08786c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "15c73fa4-8187-4bf1-be48-7c46af08786c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0b9bc5b-2b5e-41b4-a88e-35d6a1739bee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}