{
    "id": "8cca7e0e-d955-4865-88fd-a074c11d791a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_norecicla",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 3,
    "bbox_right": 92,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f963ef18-0e20-46ff-b7a7-cc8221987866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "compositeImage": {
                "id": "552c9cb7-4032-4042-a542-c8c22f68deba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f963ef18-0e20-46ff-b7a7-cc8221987866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e069b97d-8a70-49c9-9255-2c5a43507eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f963ef18-0e20-46ff-b7a7-cc8221987866",
                    "LayerId": "77bd9ff0-db08-490b-8f8c-05d92d0fa103"
                }
            ]
        },
        {
            "id": "04f93a05-377f-4800-a542-16500aff0cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "compositeImage": {
                "id": "a49afa6a-f24f-47c0-b0e6-38565a7c1362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f93a05-377f-4800-a542-16500aff0cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99bd04e5-986d-4011-87c9-89e60d29dd3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f93a05-377f-4800-a542-16500aff0cbc",
                    "LayerId": "77bd9ff0-db08-490b-8f8c-05d92d0fa103"
                }
            ]
        },
        {
            "id": "d5a52d15-6a44-4859-bad3-7dca79a46e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "compositeImage": {
                "id": "096cd0fc-8b3f-403a-b003-301e44594e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a52d15-6a44-4859-bad3-7dca79a46e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553e534e-646d-4eb3-958c-5886a3c23874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a52d15-6a44-4859-bad3-7dca79a46e68",
                    "LayerId": "77bd9ff0-db08-490b-8f8c-05d92d0fa103"
                }
            ]
        },
        {
            "id": "5b1ddc2a-c1af-48f2-bdb8-5e25e955fe84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "compositeImage": {
                "id": "bcbe2ff8-3186-498a-a0c0-d2c21c200513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1ddc2a-c1af-48f2-bdb8-5e25e955fe84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c28e02da-f2aa-4c6a-a5c2-ad96ffa55673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1ddc2a-c1af-48f2-bdb8-5e25e955fe84",
                    "LayerId": "77bd9ff0-db08-490b-8f8c-05d92d0fa103"
                }
            ]
        },
        {
            "id": "5e390c16-08e1-43bb-8b55-9b8d1a93c69b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "compositeImage": {
                "id": "142e5658-3bc9-48a7-9fa0-82c8b3711157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e390c16-08e1-43bb-8b55-9b8d1a93c69b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac303a4-37d5-4dad-90e8-44be91efeee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e390c16-08e1-43bb-8b55-9b8d1a93c69b",
                    "LayerId": "77bd9ff0-db08-490b-8f8c-05d92d0fa103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "77bd9ff0-db08-490b-8f8c-05d92d0fa103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}