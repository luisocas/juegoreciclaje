{
    "id": "8ffb5f3c-af90-4776-86cd-c8ec06eaedc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_fondo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba12776d-a275-448b-af5f-e88a5033fe97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ffb5f3c-af90-4776-86cd-c8ec06eaedc1",
            "compositeImage": {
                "id": "c68b8d4d-49a2-4cd0-95af-d5ce31d68636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba12776d-a275-448b-af5f-e88a5033fe97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f51af8-3f89-4dd9-9c85-e9a3781378c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba12776d-a275-448b-af5f-e88a5033fe97",
                    "LayerId": "2d941dc4-da71-4aea-8b5f-3b3034334d1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "2d941dc4-da71-4aea-8b5f-3b3034334d1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ffb5f3c-af90-4776-86cd-c8ec06eaedc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}