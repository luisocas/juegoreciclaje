{
    "id": "5efb2082-18ec-41f4-97ac-15aed0c6f15b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_fall_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 9,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "669eb456-8240-46bb-9b83-896518cf9d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5efb2082-18ec-41f4-97ac-15aed0c6f15b",
            "compositeImage": {
                "id": "613e935b-8848-4e2c-88eb-a33ce9569747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "669eb456-8240-46bb-9b83-896518cf9d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7723597-ab9b-4732-a808-21a47730fdd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "669eb456-8240-46bb-9b83-896518cf9d2f",
                    "LayerId": "cf6d0f02-e921-495c-9baa-5290944680ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "cf6d0f02-e921-495c-9baa-5290944680ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5efb2082-18ec-41f4-97ac-15aed0c6f15b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 112
}