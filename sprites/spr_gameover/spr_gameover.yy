{
    "id": "0e5b6b27-363a-42e3-a819-715831b1bcb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7374e9fc-3481-4db8-abcd-b4f8d37ffce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e5b6b27-363a-42e3-a819-715831b1bcb0",
            "compositeImage": {
                "id": "034055c2-3d37-4f21-bce1-8e4f6a1f5ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7374e9fc-3481-4db8-abcd-b4f8d37ffce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae53a5ec-7357-4fcb-bb22-4330ad8d9a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7374e9fc-3481-4db8-abcd-b4f8d37ffce2",
                    "LayerId": "da4ecaaf-23b6-472c-8a53-ec1b1412a125"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "da4ecaaf-23b6-472c-8a53-ec1b1412a125",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e5b6b27-363a-42e3-a819-715831b1bcb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}