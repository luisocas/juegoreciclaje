{
    "id": "2206ba65-c893-443a-8eb2-4b4c095ee2d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 18,
    "bbox_right": 141,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10067a29-cedd-4bc4-8a67-071ef1e451cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2206ba65-c893-443a-8eb2-4b4c095ee2d1",
            "compositeImage": {
                "id": "b9dd5c7d-3545-4ec5-b02a-096cfe16afba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10067a29-cedd-4bc4-8a67-071ef1e451cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d292e34-5f45-4203-81e5-8fd61590520d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10067a29-cedd-4bc4-8a67-071ef1e451cd",
                    "LayerId": "4830ec33-0dec-4c37-8c0f-42c5f5800e58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "4830ec33-0dec-4c37-8c0f-42c5f5800e58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2206ba65-c893-443a-8eb2-4b4c095ee2d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 112
}