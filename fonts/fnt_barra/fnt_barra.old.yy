{
    "id": "41ad4b57-792b-491d-9ccc-1a8609678942",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_barra",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "AppleGothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c8fcfa3a-4437-4334-a3a8-9297225a61b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "93f39d9c-025f-4be4-8646-80838bb16447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 76,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "440e2938-498a-40fc-b5bf-a3f5976db7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ad64f804-7f01-4042-b541-cb43aef81454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 51,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3ef0f3d0-18eb-423d-9724-cc9fadc5bef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 38,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "06c053d3-8c96-464e-8390-0ab2e2367143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 18,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6fb1a710-506c-4b05-ab92-55f77e01ad9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ddb21376-7ec8-45f3-9cd3-531aa16f1cc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 235,
                "y": 54
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fad7d238-8f19-463d-aafa-8e2d9acdaacd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 228,
                "y": 54
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b048de35-47df-4984-8225-b0f3a4bf1abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 220,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4f8e7e1c-58b1-4bc4-b186-fdb4842cf045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "80fe4bf3-8891-48ac-ba8e-f78bdc82bdc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 205,
                "y": 54
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "184479dc-2258-4378-9aff-13ceeb01c973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 187,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f6cb5351-546f-438d-8d1c-43622adb062c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 178,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6cf15f14-4adc-4714-b231-5ab959802fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 173,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9841698e-c123-48a8-8646-bd5f21957cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 163,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1c8daef4-32e8-487b-9538-275eb396d50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 150,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "08b587ed-0a6f-4912-8f5d-77cc9666b2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 142,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "44995a93-7e29-49db-ae40-aaf370b38bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 129,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "24f196f2-76e0-4794-a08c-631c2615ff70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 116,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1b9c4abb-80ca-466c-84c2-c6beaef33073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 103,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "63184d65-0d50-450f-9fe7-c5e122bff7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 192,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "85380d8e-8eab-4352-b434-a404358e0141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 90,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8294ca10-a652-4337-9402-d20ef64acc2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 103,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "437d140b-996c-43b9-8515-ac1f79d310fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 116,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "80d61c8d-b5c8-448e-afca-7a29ef37199f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 106
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d8e1b245-f961-42de-b144-a01c11361e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 132,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dddca76e-f085-4f78-9de0-1e2292139b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 127,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "86a15724-0340-492d-af0b-ccb2fa382763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 118,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2e7d2d09-464d-4be4-8b10-c09bb9c14d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 104,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a29b853c-0580-4b21-b4d6-96228249c667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 95,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "03036cab-6326-483d-a5bd-b61870a3d3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 82,
                "y": 106
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c6cab914-4ee0-4369-87d8-27b507154a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 61,
                "y": 106
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e9eb742e-8397-44f8-8535-fb1da1fdfa62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 45,
                "y": 106
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e7e53b21-6f52-430e-9795-7b2fb12631ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 31,
                "y": 106
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "35708945-bed2-47e4-8396-437e73d82606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 106
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "25465c60-90b8-4f3c-ab32-82f26bac2983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "18e93937-07ed-481a-a428-980e62eb40b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 233,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "020d4c4f-8724-4186-97b5-86e03bf97ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 221,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "50f14682-b888-4d99-9601-ba1b49516ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 206,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fbc8338a-24bc-4f6b-b966-1db46774bc4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 192,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3f3be7f0-2881-4e9d-a6be-2557cd78db4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 188,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "89b36f70-4020-443f-b577-c6650a98dd6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 176,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f02159bb-5767-485c-b151-02a6336abef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 161,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "44acc487-f159-4bee-afdc-590e3d3c5f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "68d9c34e-f2c4-4434-ba24-8b2677f958bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 130,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e8497785-52e8-4f39-9d53-23db8b04a2ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 88,
                "y": 54
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cef3af1c-f7b4-458a-82ba-5da53a41cada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 151,
                "y": 106
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c4b6a44f-fbf4-4afd-ad7d-10bdb5a20c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 75,
                "y": 54
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3a78edb0-4b90-4a4e-8e0a-871827c4a4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 56,
                "y": 54
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d6bfe796-2be6-4d04-b555-416af88220e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e046b558-dbbf-4351-bdde-57c71c35fc8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f09ab024-adbd-40fc-8a83-28c7d3564f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1d3f68a1-bed0-4874-a638-4ffbfb0838a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2e8c4aad-d29c-46d3-b6a0-205010aa37e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "306388e2-1435-442a-9308-4e4046b8804f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c626dc3f-b054-4864-80bf-fdb3538d6a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ea6182a5-cd58-40ee-9b9a-c10cf736dd7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f0197952-1538-4040-8601-e432d5f09aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "60837403-483d-4cc2-8a3b-84f7d57a722a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 30,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "336f60ef-033f-4042-8c40-dbade406abb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3053bdd0-e9dc-44d3-97ec-e717512005eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0ecb8b47-77a0-4e76-a4b1-a4a23b4d7732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0b8ea8af-4676-4ee6-956b-6a0b6bf6f4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "62be20fc-1abc-4013-b49d-dc32962cb66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d6d7fae7-c673-40ee-91d0-74c4bfad6eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4ec9bf03-54ef-4f72-9980-6cf8a53ba113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e9256936-9ff4-4d13-a249-bf0eddaa8aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb4e6b77-e4fb-410a-87ba-2ee4e2322ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "24f7e59c-e27f-4b22-987b-5ee78c5ca5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e964dcfb-8b0d-4fc6-afd1-76e670116f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a79443b7-bc52-4544-85ba-c31772ecb2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cd4b5a10-6e80-4bf7-bc3b-13f2ddb24677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 49,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ccbc608a-035e-4818-9f88-3dd8c379cf3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 61,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9ba41dfe-863b-4995-97f3-4629e9c24ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2394ccb7-5c0c-4964-a612-1f734c2a61ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 37,
                "y": 54
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e7fdfd21-5563-4318-b724-215818329221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 32,
                "y": 54
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "66e2fc6c-0554-4c92-be12-d362a6ebec2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 14,
                "y": 54
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "37ae97db-c63b-4caa-ad6b-b33b41dbf65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f53118fc-ccb9-457e-9379-cca4f6e1c5c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 233,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "31d60e71-5760-4ee5-a0c6-d08fa39089e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 220,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0f54cdf4-dc9e-4e3b-b0ff-c1284b4450e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 208,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a4d8fac1-2743-488e-9ea8-508f21459d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 199,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0c68996b-66fa-47c6-91b7-8483a9638fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 188,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "47787157-3336-4168-b3ec-4bd958618cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 180,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d5c0d188-9ec1-4065-8b71-204b170794e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 168,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6d56ce93-1679-4245-92a4-d4c2db61df29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9c150927-9f8c-445c-9c33-1acd84eb847f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 138,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c2411bfc-f14c-4848-b0ee-770ef5585a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 126,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cf847e91-a54a-43cb-bc18-5052d6a24f17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 113,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d9b86b52-b0bf-42f7-9dd0-4d3a5656d8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "90b4c508-3a48-4c6f-9267-bbbec4860d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 93,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "803e8971-f908-4d38-9c7b-c53ea1ce411c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 89,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1493a760-f9f7-4aec-89ea-1f61090f28c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 80,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a9e1d371-d30f-4c3d-861c-1081b252125a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "8ddfac56-b685-41a6-bf79-96c3759aec73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 73,
                "y": 54
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e920edb0-05c8-4c14-8f8d-63316eac347f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 167,
                "y": 106
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}