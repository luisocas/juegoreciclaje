{
    "id": "41ad4b57-792b-491d-9ccc-1a8609678942",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_barra",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Baloo",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ff20b65a-02cf-488f-943f-4440a8843545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b86cadea-989c-49d4-afa7-45554ed1b84a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 110
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "22ce6122-e6ea-48c1-a5a6-3de86d17ecb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 110
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fb26b6a0-2d4b-4c0e-80c2-b7afcd906dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 51,
                "y": 110
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "51302696-6bfb-445a-a8b2-265e423ddd8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 37,
                "y": 110
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e6838d80-bab9-4220-a04c-593231e22d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a7b84531-c7bf-4244-ba81-826397f417eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0f2f565d-d21b-405d-8f93-d3d7612a3009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 245,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5da63f90-8316-4996-8bdf-0113881c242d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 236,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8eb284f7-69f6-4134-a3b7-b8ae5ca226d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 228,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d22000a8-3f9e-4e8f-a047-429cb4390b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 86,
                "y": 110
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d94bfc3b-54d9-489b-9cbb-b0ba829c6df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 215,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b215e157-2306-4ff7-b85b-81ee3db9d5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 194,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "089187dd-2c44-404d-8399-4794c84d68e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 185,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "de0809ef-7d73-4024-ace7-1c9c30dee56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "86540d25-4d6d-4433-9bdd-fe2a7f381436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 168,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e891ce72-d379-4c92-99f5-9fbb4d864a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 154,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "80888ebf-fd37-41e7-94d0-479c69db600f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b9276501-288d-43b9-a7fa-5f1e47370224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "63ee07f8-9487-404c-968b-1c9d16cf5f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f7f93d48-f503-4bc8-ae08-6194206d2b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 104,
                "y": 74
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fa310d34-bf7a-41bd-aa6e-df5656a759cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 202,
                "y": 74
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "08b1ed24-8a90-45a6-9e07-5c52cb0922ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 95,
                "y": 110
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7e323ba2-7147-43e0-92a9-72260c8c383d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 110
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "49b4a2a7-bcbb-4f65-80d9-c3090bde8a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 121,
                "y": 110
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1656fb4a-09f5-42f9-98fc-377ebb9b6c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e54f9b4e-1639-429e-a0d0-900171702af7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 138,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9197aaa1-ea6f-4cb1-8240-c0f75a84fdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 131,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5bff092f-6d13-47ef-b88b-55b7cfab6452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 119,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "46ab08ef-04dc-40e9-8b0a-3babe81d49b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a376791a-53df-4cfb-959d-155a70aa45c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "02b87d7d-84df-4078-af72-aa946595e480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 83,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "50cd481d-3a9d-455c-841f-aa6e4ef4f7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 60,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1c581d37-b1ad-4b61-8c75-ee78a97b0292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 45,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "27bfba1d-ff60-4ba7-abec-288f5724f9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7c7d61af-ca61-4848-9e05-282818d67fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "923683d1-0b51-453e-9acd-86947b432ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4db519d6-9b65-413a-9534-e98313d37512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 235,
                "y": 110
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d848774b-b0ec-4d9b-b311-857397e56eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 223,
                "y": 110
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e29d3110-8ab6-403f-95ad-08673c195b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 208,
                "y": 110
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "44a5d3b1-fafd-473e-99dc-8b6ccfdad634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 194,
                "y": 110
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "acb7071e-b7c5-4397-a23d-d6fa3cdf6384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 187,
                "y": 110
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "49181ef5-76ca-4a9d-875a-beb6a9779e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 110
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2e7ab062-ea6f-4524-9e0f-6dfe5ec0666c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0be53d7c-815c-4a6b-9fbc-700606b85a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 151,
                "y": 110
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c673d8e0-1a22-40f6-851f-ca558cb7ddc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 134,
                "y": 110
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4368b6e9-1cef-46a4-88d3-00de3dc7a8b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0e08935a-e0eb-497b-b6b0-d0c0dee7db76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 73,
                "y": 74
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2e17db07-5185-4f8d-bbd8-e3af299897f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 60,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f946adbc-ded6-4169-8038-049fa32a4ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 25,
                "y": 38
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "864165cb-9f58-4ff2-b6d0-1989105c68fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9e995f59-ca4e-4ea3-b52b-765ade95e668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9fcf25f2-8637-4f68-9c8e-a8c743ce1f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e39cd567-eee3-4b13-9979-6f551ae983e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "eea4ec7a-33db-4c24-8778-8c895578c9f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9983e4d2-4861-4a06-8a4c-e24795b4ae9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6cdd7a5d-4c09-401f-9420-79b87a289241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6c1d622b-0ea1-45c3-90ff-f104ccb46f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "319176e3-96cd-46da-bbde-07f5354979a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40e7786d-7bf8-4396-b064-e94d0cd36ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "71c5e4b6-9a3e-4f58-a016-9d2a4b840fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3aac5283-94c6-499e-845d-51a1f099b4a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a29d855d-dfe8-4ccd-b335-38a888fa6c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "34ee952c-d672-4441-b511-555449bb1926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "35df6c33-7668-4000-9942-c413350c23d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a5e92522-ce58-43c1-9afe-021bffed6002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f3a1afdc-e344-443f-8b2c-63428ed64dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4acf42ae-dcca-47fd-bed2-e74f5139bb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "49e0fee8-35a4-44b7-acc5-64b55b5ae812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bfa2f7da-15a7-417d-b0c4-6ee55cf3eb55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a5e0226b-00e7-44ba-b163-2714fae0f19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ac5d23f5-b455-442e-9bc6-1569959185d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 42,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4d61212d-5de4-433d-8392-c18b8aefe11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 169,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "73e22f0c-1964-49e1-ae0a-0f9b247aefe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 55,
                "y": 38
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fd483385-d866-4862-b5c3-772c64e670e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e4a6faff-2f88-4eee-b779-63051b7ed562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "69eaf27e-c232-4d0a-8bfc-d0a3cd0ec908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "47a9caf6-6dc5-48fe-986b-975473ff8585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "08da948f-bba9-4d81-b7c9-acdc5afbfff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 242,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b96062ca-abca-4022-b83a-71b72672c1cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 228,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c99ab63a-b2b3-4c08-a4f4-129937e5b4c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 215,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "221de20a-fad0-498a-9288-4e653561bd09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 201,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3eee8781-35cf-48d8-9a2c-bf988d0ee3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 191,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "73944acb-3121-4474-8ef3-b359fee767f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "51488d67-6d42-43e2-9f0e-2a3e7c0a33dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 182,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "739ee165-70f0-4eae-920e-aaeaa6d7f482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 156,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7d60149c-4fb3-40b5-8a1b-2acafc66e0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 143,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "34319bbb-945d-4fc1-8e70-99e0e4d7da71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 124,
                "y": 38
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6e8fe5c2-2936-480b-a6da-99241edaf995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 111,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d936f2a1-4555-46c3-82d8-a995e1f76d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 97,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3aa4e5a5-bea8-4666-991d-5ecbc172373a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 85,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "75968b96-1613-4b9e-8199-057927e80a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 38
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d49298fc-8613-41ce-9517-745d8b6f5ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 70,
                "y": 38
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bb163178-9ba4-4d4e-975d-977785c43211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 38
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7f39bcf6-d2ea-40bb-97db-7aef4dfeb27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 158,
                "y": 146
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b0fa0790-e409-4745-9719-4f3184852de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 34,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 171,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}