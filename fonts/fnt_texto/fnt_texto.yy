{
    "id": "08cad629-f4a9-495c-96cd-7fb58d4f922d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_texto",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cartoonist",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8e42b9d4-2e3c-4030-a86e-fe3c521c7fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "eff7c6ad-e0b8-4bde-9551-5c651d807f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 60,
                "y": 134
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "68006c58-df2b-41de-a214-827672372e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 43,
                "y": 134
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fe125842-b617-4403-9b8b-d1fe088ed743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 41,
                "y": 134
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "23faea78-32f9-4c9c-a033-5fa0457d2d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 39,
                "y": 134
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a545b9d8-d953-4077-8884-e6b9dd202806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 37,
                "y": 134
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3d7d5a71-7e9b-4829-91e9-2148a87129f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 35,
                "y": 134
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d4d6050f-2b4f-4632-ac40-c47ac5b7b6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 27,
                "y": 134
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "184a6170-50f4-4512-94ea-8bf5874ab333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 25,
                "y": 134
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5fd02980-b796-4b1f-bd3b-893b74ede54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 23,
                "y": 134
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0d5dcb96-1a00-4b5f-a4ef-6a4129ec7ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 70,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7fb5bce1-7bcd-4985-9d7b-03db0801839f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 21,
                "y": 134
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c386504-baac-47c3-833f-ca2e9d53b06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 228,
                "y": 101
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "28b14beb-afa2-4840-8ace-ae94db57b38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 226,
                "y": 101
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "82c80b22-e68b-4da0-88d5-4f51bfd1a84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 214,
                "y": 101
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f7d53541-68ec-4396-9ecb-cde99711bf3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 212,
                "y": 101
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4e41fbfb-b8c1-48ba-85c7-6ef1c82192ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 193,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4933851c-72fc-4d26-b8a4-bf8bff4b265d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 181,
                "y": 101
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "57aae9df-4483-4aac-a241-fa40c369ea42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 163,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d5d19a46-81d4-447e-9539-fa03a105abb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 146,
                "y": 101
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "66f32ca5-c1c1-4ee7-96cf-00bb8e0349d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 127,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "72ff0ac3-a0be-44e2-b6f3-88a8ae5514b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "595d62f2-b23d-4923-a122-ee9fae62d60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 72,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e967af80-f33b-4dde-a2b7-7001d6595160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 89,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "51f8d3ca-aa14-45b2-9904-124ea3d8b70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 106,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12d34c2d-9c1f-445a-9316-77c3e5ab0816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 156,
                "y": 167
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7751a90b-5c37-4386-849c-d9820507f0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 143,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a2a1d167-10b7-410b-b684-dbd898fe0f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f1a5f4f8-e44a-4d75-afb4-5b57cf51331e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 130,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "66bf2e04-4977-499b-ba20-958a1aa55716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 128,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fda8cc8d-f2cf-485d-8d2f-9be5465cf85c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 126,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6b12a71e-7366-4bbf-ab63-82601a915d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 109,
                "y": 167
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e840d01e-645f-486a-9211-400d2b645535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 107,
                "y": 167
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "20024605-f8a4-4cb8-9eda-92caa4c771e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 85,
                "y": 167
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1591d14e-9c7e-4102-bab3-ac978c10fc85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 68,
                "y": 167
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0b438f41-5d1c-4ba4-ba12-551c20bc1a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 50,
                "y": 167
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "35001576-2959-4786-aa4b-32cc080c8638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 33,
                "y": 167
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "65257e05-3e1f-40bb-90cf-dc7a212c0918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 167
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c58cd5ac-2026-4ea1-8211-2a18505a80e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1fd2c3eb-eab2-46be-8ff1-bd957488bb6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 225,
                "y": 134
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d7e37e9e-fe5f-4e4f-bfb2-7f3c7c1d5965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 205,
                "y": 134
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c0cd0777-036c-47aa-aa88-df8ff579981c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 134
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "fcb1b798-28c8-4065-a87d-9e450bc4b217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 177,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "beb01672-612d-483e-b1ba-20bc1f4575e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 158,
                "y": 134
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9e640ed4-6109-4fb7-bc62-3cef8b249de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 144,
                "y": 134
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cf13ff2e-da35-4914-b57b-51288b8d2116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 123,
                "y": 134
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "62e36177-6fc4-4176-b269-baefb9e3b26f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 108,
                "y": 101
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9d23244b-e15d-4bd2-89cc-0af58228d84d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 89,
                "y": 101
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "594e0d23-9750-4ff7-823d-c8387a05e9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 72,
                "y": 101
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "aaf9aae3-21c3-47b6-a58f-234d6b99d750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 125,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "54494dd2-b8b1-48fd-9017-b602783d1d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 105,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "938ed9c5-b346-4322-9970-d6ae2b4cf120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 86,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6ab65a1d-27ce-4958-a0a8-22a86e687a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 67,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "53851fe0-3d1a-43e2-bfd5-f7b5a5dcd651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 49,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "364fe471-e2b5-491a-bc05-15fe61591287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 29,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0de67dc2-3b6c-434c-b6ad-f77ff03331b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6332ba8c-3178-4080-b0a7-731c5f566baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "056374d2-840e-483f-aadc-e400ef7988e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "47c5d964-1656-4e81-8388-bb4499096ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a40eb81a-0cc6-42d1-be39-b24f397016ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 123,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3a68f341-2c21-45c6-ad4c-bef0e106bf40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3bd6383c-ed60-4b29-90a9-da0598ba9ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "650671f6-919d-4b43-93a1-a4888d591276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c75038cf-be95-4ea8-b5dd-d3cd8d9f2d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8ec3ae5c-14bb-4121-b019-bbb19b29ccde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0e4a06f2-ec86-4efd-8514-6f6efa3eb126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7272d1c2-1f56-41d1-82ab-8667864b5af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cd5ce1d5-d94b-4280-9a5f-89b90e2cdd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "196017b9-61f5-45cd-8702-070d9722409f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "86a9ebcf-ed97-4be2-aa0f-3617c8593dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "05bb9681-7fc4-4686-984a-46559da2f8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f4887ae7-2828-44c8-9f92-fe038e0e4f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 147,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d764c1b6-6b90-44bd-925b-7b6a9b09ac8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 89,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bdc9cc22-8b69-4268-ad38-37174de1adb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 168,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "be41037f-3db6-4dc5-badd-89b192d719a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 35,
                "y": 101
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "24203dfa-8b4e-43f1-b9ee-86a9375c7c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 16,
                "y": 101
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7c6f8f58-e245-48be-a076-d1ada0d14cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "93caef8c-73bb-4a90-a01b-6ee2eb2c1307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 223,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a33c4b72-1841-461d-9e06-aadc2bdc0215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 204,
                "y": 68
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "87edf575-4dea-4605-a004-401d4f63c617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 185,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0103688f-e45c-4919-92e4-64c512b32b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 168,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "44b3f508-bcdc-4ed0-bf03-1a5cbdac246a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 146,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8a39fac9-bbea-471d-82bc-82b2ad4008f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 128,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8ee428e7-0b59-46a0-bfc6-5c9c73b1c1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 53,
                "y": 101
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "df0e9ee3-69f8-4faf-a60e-322f0394ef50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 109,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f0252415-8a6f-4b62-a0d2-5516b19b7359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 71,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "469a01c1-37e6-49c9-9cf8-4397e180bc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 51,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3faa140c-3653-431c-a9ed-21aede4dc5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 24,
                "y": 68
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6d2a63e8-8630-4f7d-adfa-91781e3771d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "af5f2d9e-d60d-4d17-94f1-22e04a3cacc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 228,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a80fc18a-cd8e-466a-bf5f-4d73280a4403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 211,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "582027c8-5f12-4e94-9d86-52cabcf851f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 197,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "091ab5e6-7048-43f2-b60f-4701f7cd972a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 7,
                "shift": 17,
                "w": 3,
                "x": 192,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "201afbab-bd13-4b00-b017-d6655e7f041f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 178,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b1ce14a8-6234-421f-aab1-e1910b45905d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 174,
                "y": 167
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c141b720-6a25-474b-a3f6-08a032df16e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 190,
                "y": 167
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "71ef0c6b-9b1d-40d8-a47e-bf433e1f8355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 44
        },
        {
            "id": "791e9dc2-f206-4284-a84f-549f99af595e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 46
        },
        {
            "id": "0c29d183-9b4f-483f-bffc-94e96f90bdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "00af902d-3727-4514-b358-6d59b1ea3a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 56
        },
        {
            "id": "734481c3-4db1-46e5-aeda-cb1a5eccb254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 63
        },
        {
            "id": "c67b8e20-bb08-4513-9d00-673ec616e62d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 65
        },
        {
            "id": "e8e3b115-2e94-4eca-b558-95d6d45035eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 33,
            "second": 74
        },
        {
            "id": "463933e2-dbd7-46e1-b924-121fb4cd2524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 83
        },
        {
            "id": "aa26917d-7895-4a92-ba42-4dc1fa07ba93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 88
        },
        {
            "id": "2abdf1fa-8a80-4241-a09f-b07cb9b416ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 34,
            "second": 44
        },
        {
            "id": "cd699945-1980-4c11-9cce-21f25c901b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 46
        },
        {
            "id": "947c7a99-653d-4823-b668-67ceec298e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 48
        },
        {
            "id": "e3cef9d0-2de7-4e1f-8c54-11f9c13ea4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 52
        },
        {
            "id": "a03ce929-6324-4bd9-b01d-5439091301c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 53
        },
        {
            "id": "af187ce5-c68b-454b-9aac-7f92094ec62f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 54
        },
        {
            "id": "96800d69-2b15-4936-b4fb-19d64828131c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "069ef03e-576b-4973-af6d-a758d22ef717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 65
        },
        {
            "id": "e2b76321-8584-49d9-8316-fa5ffa8da460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 67
        },
        {
            "id": "e0325227-c636-4a1e-8995-77bd85d78995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 71
        },
        {
            "id": "2d6d5831-0ef9-4b5a-b6cd-5a95d5ce2b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 74
        },
        {
            "id": "0206c8a4-86a2-406b-9755-c5ea4b507cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 79
        },
        {
            "id": "d5f276c3-5d97-4578-bad2-c01f39261e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 81
        },
        {
            "id": "e9a3a73f-dcbd-4d8f-8f98-61229a03ec1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 83
        },
        {
            "id": "8eaa9d2d-2dbe-48cf-a518-c766218d5c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 39,
            "second": 44
        },
        {
            "id": "1157f3b5-292e-46d8-96dd-fd01e5d2872c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 46
        },
        {
            "id": "2819bf05-3a01-4649-a6f1-bb5414bf2deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 48
        },
        {
            "id": "fdd888a5-7cc4-4a76-b5d8-085ae3d021ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 52
        },
        {
            "id": "09168446-5b1b-4731-b716-c5038783374e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 53
        },
        {
            "id": "78d4c932-0eae-4c54-aec3-825dec98f087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 54
        },
        {
            "id": "7d1806c6-1847-4c91-aa47-f7e260fff135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "37de95e8-8ede-4976-8202-fb38a64b0306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 65
        },
        {
            "id": "404f8348-5c10-46af-916a-707db3294680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 67
        },
        {
            "id": "8287505f-e628-495e-b1c1-102dbd1cc47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 71
        },
        {
            "id": "8ca5d822-a38d-4a9f-a27e-82a89f21970a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 74
        },
        {
            "id": "cf6fdcd3-3cad-43aa-85fe-a240f1780d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 79
        },
        {
            "id": "61c67262-e03c-46b9-9b53-4b8c1933b081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 81
        },
        {
            "id": "acc3318d-5b07-4960-8320-f4a2bcc7a1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 83
        },
        {
            "id": "56db11bc-533c-426b-9914-8c5f7b768e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 106
        },
        {
            "id": "47384016-0980-45c7-844f-27de77c6360a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "8fcf676c-7429-4e7e-b991-62f342e7ce70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 44,
            "second": 34
        },
        {
            "id": "e5d5135d-820d-4a0e-8df1-2d88bc743ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 44,
            "second": 39
        },
        {
            "id": "5e672ed1-b2b0-4bb9-9ad6-e02780c90ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 49
        },
        {
            "id": "a2d83817-f010-435e-9d63-47cb2371089a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 52
        },
        {
            "id": "f36d7335-0bb5-4165-877c-f13bbbe738c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 55
        },
        {
            "id": "6bd87621-0631-4fff-98b3-1d0bc21d4c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "12dce641-11bd-4e99-a853-cc2f6dd77737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 63
        },
        {
            "id": "b3d4de05-2c57-4dc1-9546-3c5e44177c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "2482f6a9-9fbc-4125-a880-695fa1e37c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 72
        },
        {
            "id": "fae6403b-5d6b-437c-b745-c30e4cc321e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 73
        },
        {
            "id": "c400027a-1bba-4eae-bde8-15f90e1d92f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "104b6945-cfbd-47d4-b08b-3c3091afc223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "c2c03d87-ff55-44de-8d61-71af79e1ebb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 86
        },
        {
            "id": "8a31c098-b94e-4358-aca9-1de758b6b472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 87
        },
        {
            "id": "551ee409-5356-4e76-98fe-ca71e495dd78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 89
        },
        {
            "id": "fdf843bf-2278-4350-b7b7-929954a31767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 90
        },
        {
            "id": "4c80a09f-b7b2-470b-9423-a30878b0af0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 46,
            "second": 34
        },
        {
            "id": "365d3657-1fb2-4100-aa18-cdc6036b8a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 46,
            "second": 39
        },
        {
            "id": "93d64eb4-5325-4ec0-a3de-9e29ade9ef80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 49
        },
        {
            "id": "1ba368f0-c52b-454b-9703-5d6e1d395581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 52
        },
        {
            "id": "2f90fbd7-c0e4-4029-8a20-105320c00fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 55
        },
        {
            "id": "3fc8e467-9b14-452f-a05b-e9742c8c97c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 57
        },
        {
            "id": "5661d5c4-3872-4d8f-abbf-5bec93dac744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 63
        },
        {
            "id": "42201fff-4729-407a-a476-4d6ab085efc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "6c7ac37e-a309-4f38-b5e7-12aeb424a2f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "b6f9be23-2f18-4f7a-bcf1-ae4568293d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 86
        },
        {
            "id": "6fb33185-fe08-490f-947a-659a267d89d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 87
        },
        {
            "id": "6fb3dcc2-8384-43aa-a00e-8acf429b4edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 88
        },
        {
            "id": "d47923dc-45c6-449d-b9bf-5767d430fbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 89
        },
        {
            "id": "a410cee6-36b3-4eb8-ad24-c078ba166530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 34
        },
        {
            "id": "131cdf37-157b-40f9-94a8-c5964d4d9c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 39
        },
        {
            "id": "d5c9146b-eb48-4049-9515-c3cd15b6a22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 46
        },
        {
            "id": "b6b1ccea-a33c-4ceb-9834-9f9481ea4443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 49
        },
        {
            "id": "6afec3c2-3baf-4bb4-a59d-68d829088409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 58
        },
        {
            "id": "2bc734d1-3bc8-4969-a269-ac8867ad6622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 63
        },
        {
            "id": "f5a64846-f3d2-4841-be8f-1f1000b1035f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 65
        },
        {
            "id": "78373f15-dce6-4f3b-93cb-a8b3ae8319d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 72
        },
        {
            "id": "9fee03aa-1ce0-42c6-8540-f83ac09f0d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 73
        },
        {
            "id": "012ca88c-3990-4bf9-9877-1e8b96e7cdf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 84
        },
        {
            "id": "8a3479d1-119d-4418-bca9-f7e7a7aa2ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 86
        },
        {
            "id": "1ac70819-c399-4da2-9e96-3cf9f65608c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 87
        },
        {
            "id": "a268f341-86ab-421f-8bce-6399f28eafe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 88
        },
        {
            "id": "c83128bb-be1e-4ab7-adcf-0c823d5abc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 89
        },
        {
            "id": "41026a57-874e-4b9f-b58b-7d39b94e810f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 50
        },
        {
            "id": "edad9e8a-d33b-4201-9600-7226713d8742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "6baa438d-58a4-46e7-8daa-c1ee185fe732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 44
        },
        {
            "id": "66d6a129-0fa4-42fb-a269-bcf1874c00b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 46
        },
        {
            "id": "1ec465bb-1249-43e5-93a5-6c6861e93c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 74
        },
        {
            "id": "6c29f6ed-10cb-46d0-bd07-bab6e4659ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "31a30429-9a53-4778-9047-4e2014ac0651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 89
        },
        {
            "id": "f9b9f9e4-b278-4331-8fe0-1269a7cc1bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 52
        },
        {
            "id": "758ba58f-84cf-4487-88f3-e90cdd0f26ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 65
        },
        {
            "id": "836d41fe-1a83-4ab2-8c38-276af92cc02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "c2edb938-c89a-4c82-bafe-3315e025d0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "ecc2234e-9718-4e01-bacc-0bc2678b7117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 88
        },
        {
            "id": "3f22f76d-3546-4db7-ae96-847a78ad2224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 89
        },
        {
            "id": "c4ffc50f-3796-4b68-b1af-42aa4cf293d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 34
        },
        {
            "id": "a1536858-163e-409e-aaef-91d048a9f37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 39
        },
        {
            "id": "d1f5d9d9-a5e6-4199-8191-a22fd361f8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 44
        },
        {
            "id": "28e064e7-ae34-4a24-a4fd-4493f3bfe428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 46
        },
        {
            "id": "7e93bd9e-362e-4ac9-9989-63c6a7b52f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 53
        },
        {
            "id": "28722854-6164-48ed-93a0-33326249b462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 58
        },
        {
            "id": "c05567f3-d4c1-4443-8132-8a29383afad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 63
        },
        {
            "id": "258c657b-30e3-4b91-9911-31b10e6bad69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "a7d03257-4cad-4136-8b08-99de0cded975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 83
        },
        {
            "id": "8707be76-d213-474b-b49e-12c9adda79ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 84
        },
        {
            "id": "b37ff2b3-8943-415b-9461-0e49ee833d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 86
        },
        {
            "id": "31e1550d-ad25-438f-a6fc-b09c05a0168c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 87
        },
        {
            "id": "d9811727-aad8-4ed2-90ac-8e4f3eaab13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 88
        },
        {
            "id": "6b96a5a6-d3b4-45cd-958a-023272022688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 89
        },
        {
            "id": "9f2333af-34fc-4834-ab1a-f6f673d52873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 63
        },
        {
            "id": "9e8ccab4-4431-4bf2-b8a1-938ca6232cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 65
        },
        {
            "id": "e1083acb-a85f-47d6-8b33-ac73aa6e828c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 72
        },
        {
            "id": "5757c9f5-0ad3-45a4-b4bf-3c326539b153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 73
        },
        {
            "id": "f4a870dd-5a90-42ad-a9cd-40ef9d3baa73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "36f45071-b4d9-4853-9eef-33d310019811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 88
        },
        {
            "id": "b9c59b6e-784e-4fc0-8923-bb167ebecf8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 89
        },
        {
            "id": "6e0f39f0-197d-467b-8210-2505adcfd0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "0ece2bbd-b56f-47bf-9a8f-f3e35a9c42d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "1346a5bd-0048-410c-bb48-88dfa31458ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "77358ebb-7852-432a-834d-f89f05c3184c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 88
        },
        {
            "id": "81f76ff9-09c2-4432-bb2d-629e7bcd1896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 89
        },
        {
            "id": "72ec3eac-1ec4-4945-b7cb-30323765a5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 34
        },
        {
            "id": "7cfc9a32-9214-44d8-8bf2-ac5a52d449ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 39
        },
        {
            "id": "99dbdb14-be5a-4b40-990c-681c239adfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 44
        },
        {
            "id": "d44d2d6c-ff3d-4b57-bc32-b70a79646077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 46
        },
        {
            "id": "957a35aa-d74b-4265-ab15-8b6604ccd57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 65
        },
        {
            "id": "0ff99b9b-0e44-4675-87c4-1dbcde346e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 67
        },
        {
            "id": "016ca41c-e713-4c07-839d-c27fca441608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 71
        },
        {
            "id": "c5a00ec1-106a-4e0d-9b2f-db342d5f6ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 74
        },
        {
            "id": "9372f0a6-a94a-4206-80ea-70e2a0042909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 83
        },
        {
            "id": "3191497d-e88f-4cd5-a33e-2496f047d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 88
        },
        {
            "id": "7f6d0049-3b36-4238-bb6c-45cbd707f8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 57
        },
        {
            "id": "4266ebc4-38cc-4193-88b2-415e69f699fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "091cc425-c046-4b9e-bb43-d08822656eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 72
        },
        {
            "id": "9f75be7d-4925-420d-9612-559f5d044872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 73
        },
        {
            "id": "376c177e-e047-4757-a05e-22ea104cb2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "fb3cbc2c-487b-4c2f-9df9-aa4761b2132b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 86
        },
        {
            "id": "336c0d64-b923-4a78-850c-0b9acfe87d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "e8a452e0-2044-4625-96ac-a8883b6340c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 88
        },
        {
            "id": "c41f7a1c-2029-40ad-b808-2f0e5f5f4f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 89
        },
        {
            "id": "42cb0989-efc2-4622-adf3-b86ac7e2f0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 44
        },
        {
            "id": "f921e3fb-0a1f-4405-9300-a5f87a870a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 46
        },
        {
            "id": "50baa515-a885-4fe6-9a01-777e51e893b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 65
        },
        {
            "id": "b8dcf0b9-7b93-4df4-bd9f-db7b16db2b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 74
        },
        {
            "id": "f13b5ed2-8d4c-40a6-9013-51e48b977f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 83
        },
        {
            "id": "4a432d12-d3ab-4ede-b856-65d212135daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 86
        },
        {
            "id": "1e3839de-a938-44f9-9aee-feece854f92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 88
        },
        {
            "id": "413658f5-f297-4c39-be88-abd5240fa076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 89
        },
        {
            "id": "5fff89d9-6d2f-4b94-86dd-288a46a0e66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "2c0486af-c2ae-478e-be39-f49d9095cbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 63
        },
        {
            "id": "49f90320-3373-44fd-a7de-b0e79372602d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "2925726e-2d6d-40b5-8389-92cc6399b5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "563e0d62-4e49-4675-a105-8dc6b02d4b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 86
        },
        {
            "id": "5a200a16-690b-4146-8860-e1e226bc6b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "d46ab4b2-2752-4981-92a5-3084dfc80f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 88
        },
        {
            "id": "a1bdd6c8-ab1a-4f9c-b2b0-ea4e314bbf24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 58,
            "second": 89
        },
        {
            "id": "2c2dc815-dd52-46cc-ade2-6aec21a04cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "1cf9521b-ba5e-419f-91c5-aef1f3a47388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 65
        },
        {
            "id": "f0184385-4c8a-4d2d-ad4f-f5a7ffb7a6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 59,
            "second": 72
        },
        {
            "id": "8058e0a6-fdd9-4e37-a7ea-b33fb00a5dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 59,
            "second": 73
        },
        {
            "id": "78309a20-601c-4a01-8db1-3d1204c2d7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 59,
            "second": 88
        },
        {
            "id": "0edc8c4e-cf0b-48fd-832c-1f91b160d656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 59,
            "second": 89
        },
        {
            "id": "156f6409-2be1-4a9c-9ae1-6cdc7a2c9fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 44
        },
        {
            "id": "928679fe-bb81-458b-9eac-05c889a7ae5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 63,
            "second": 46
        },
        {
            "id": "7e4ec3b2-1995-4775-b6f8-31d1bea0862d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 52
        },
        {
            "id": "f13e0597-6c23-477f-a78c-7543b2cb0a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 53
        },
        {
            "id": "975d7a17-eb55-465f-b860-36ade4e16560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 65
        },
        {
            "id": "9d6c2041-d55a-449d-bfd9-3fd0992df5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 63,
            "second": 74
        },
        {
            "id": "4626cddb-0dec-4dd2-aa3b-09d018ac8834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 83
        },
        {
            "id": "c78ba16e-0f37-47be-b815-56006d26b8aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 86
        },
        {
            "id": "f758c8bc-0d2f-46e8-8a78-fab69ed59a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 88
        },
        {
            "id": "3cbf16bc-97da-4a50-8683-50d39fa09968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 89
        },
        {
            "id": "7405a92e-f72d-46bc-8016-1e5b826b85b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "ce7b5883-2d85-4d57-9fb0-e3a2ca5e3e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "f2bdd929-5f68-4c79-8cf6-e235138fd9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 49
        },
        {
            "id": "5f7406c2-719c-4567-a59b-01ccd1561351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 52
        },
        {
            "id": "26ae2a5e-ee28-483d-afbb-c1f36001ed23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 59
        },
        {
            "id": "da05fc18-f134-4c35-9aeb-05a55eb439fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 63
        },
        {
            "id": "f20956c3-3a70-439f-939e-ac2fc1a8a3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "cc75f03a-09aa-4626-b0c6-a694e67cb332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 73
        },
        {
            "id": "ea261505-d706-454d-903a-cdbc409d635d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 74
        },
        {
            "id": "2d1b0b44-93c2-4e88-8743-a16605b793cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 76
        },
        {
            "id": "b155afaf-0bb6-4ce5-99c1-e1667a57e5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 77
        },
        {
            "id": "9b33dc28-0d62-4682-b51d-7deedd6db5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 78
        },
        {
            "id": "a71b07cd-1a16-477b-b4a2-06f60bf4fe58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "6e2c902d-6af1-437e-9e9a-ec91e503f403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "36502e20-3c67-436e-bea0-458923593c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "7c39f11d-0b61-4e42-ac31-bf8b16653c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "de8d64a5-21c3-4f2d-97bc-13740f4852c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "87b11522-0ccd-4bd9-ab05-9279a788c871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "79116844-1ca7-419b-a8b5-7be2c19d5f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "b4e5c19a-ef1c-4257-a45d-a239bd0c60ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 118
        },
        {
            "id": "934a1c8e-9029-4e23-9fe9-d4a7a94ed174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "4bdecd18-ccd5-4754-9d15-4be0265c5995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 121
        },
        {
            "id": "aa9f3149-e33a-4fa4-a735-cec36f0e4fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "05f4bbb2-4ae1-4a1d-ae69-20f218257f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 67
        },
        {
            "id": "b169f505-32c2-45cd-888e-f8329cf16e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "02d1dcb2-d57d-4fbf-ab27-52ecc0a23fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 73
        },
        {
            "id": "c18fa593-d8b7-4567-b1e5-4d4234654e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "d3506bd1-55fa-4396-8c4c-83a4bb26e328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "1c0badfe-b1b6-4a94-b128-b3431ad6ca58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 86
        },
        {
            "id": "8e546da0-96e9-4eeb-bc67-aad57a8c0440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 87
        },
        {
            "id": "a377244f-cbf5-475f-a098-b91f4b0df95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 88
        },
        {
            "id": "23bf2504-624b-47ea-8d61-2f5735859a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 89
        },
        {
            "id": "78c76297-a36f-4539-aee5-99820dda1035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 34
        },
        {
            "id": "2c31cfd2-e1a5-4982-86c4-94b939b17b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 39
        },
        {
            "id": "354640a8-fb87-4f33-85e9-923e2cf483cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 48
        },
        {
            "id": "a17de1a8-1cb1-4d03-8ad3-6b582c54ddb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "33e8a2d9-6bcf-4168-a7d6-7aec6bc97e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 52
        },
        {
            "id": "106d68c2-9653-439b-af46-ae0ec01d5c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 54
        },
        {
            "id": "4108d190-6ec9-4e77-896e-53846b747de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "a13b7d83-f109-4d38-ba2c-eeae20a7f72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "7aef9244-03b4-4117-a68f-459788acc03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "73e90726-e816-49a6-9a82-12ad608e7228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "9f5220be-83e3-4d2a-8ca5-bfe3c3558c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 74
        },
        {
            "id": "0f1fa71d-40fb-4cdd-b0d2-59bb3a709704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "aae4d603-e661-4f57-ab35-f5119902a377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 83
        },
        {
            "id": "04bda119-c485-4d77-8e66-72e078524e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 34
        },
        {
            "id": "a92884d7-9e02-420f-9d0b-c5aef7709bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 39
        },
        {
            "id": "7d0eada0-f9a1-40d1-a1d7-6ef50da16ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "2863a371-885f-4e6d-9202-856435be6902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "c30365a7-b440-431b-affa-e5494be8a16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 51
        },
        {
            "id": "055c57c7-2960-4c85-9285-6894a8b71fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "a6197eaf-b4f7-4e30-8019-85265b5b13a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "938ca474-2505-43f0-9e57-4377d174ab3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "d5d09b4a-3823-482c-9403-5f4629d3bef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 69
        },
        {
            "id": "5b35b8d6-4434-4783-8847-998c896ee6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 72
        },
        {
            "id": "c8ac960d-5f50-4ffa-a4f3-a68bb1f8d6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 84
        },
        {
            "id": "370ecee0-9822-462d-b927-02540734517b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "fa2bfc94-f356-49bf-a1ba-dc6c5d8d4cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 87
        },
        {
            "id": "294f24e8-7f4a-4714-aa6e-34debabc54b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 88
        },
        {
            "id": "c47624e5-cc91-46ad-afa8-dc9221e0d2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 89
        },
        {
            "id": "d99c4bb5-198f-49e8-af6e-b6fbea93f83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 70
        },
        {
            "id": "f4ee7cb5-f339-448e-b3d8-74f2b3e1f9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 72
        },
        {
            "id": "e6c7e3e7-3254-49c9-b659-6189ecd74755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 73
        },
        {
            "id": "9af889d0-de43-4c17-b995-c908b9d2a536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "fd4c7db2-af8c-4aba-a2bf-e316803b66aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 90
        },
        {
            "id": "dbac98d9-336a-49e9-a235-9785ffc18f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "97f0e464-2152-459d-bd2b-7a9684fb01aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "6dd64232-42c2-48cc-92e8-72c098180a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "ff68c300-6615-4fa1-b6be-9a833990a216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "b5622b03-1d46-425b-8be1-34e623380357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "4134b9a1-4efc-4724-82de-68bdb80a607f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 111
        },
        {
            "id": "b6aafcee-cfca-4c28-9056-85a3411fd17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 63
        },
        {
            "id": "86b07b83-cbba-450d-b19d-2ded9307e3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "229b07db-6b29-4414-a647-7286f28f2906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 73
        },
        {
            "id": "90ba6f88-4d3c-461b-9666-63d9413e8862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "95f52013-601c-4102-a5b9-f7ee4d912a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "1aca5ab0-6d16-4f02-848c-7fb65b528923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 67
        },
        {
            "id": "5e889671-bdc5-4db3-ba52-73f5ef42d099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 72
        },
        {
            "id": "1c58016b-eeeb-4aa5-9c7c-e45511e1c3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 73
        },
        {
            "id": "ba06e3b6-eecf-4de9-89b6-78da4091e448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "fb84b3b8-7a6d-45f2-b778-c05aa7a3ec1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 44
        },
        {
            "id": "10ba256a-34dc-411c-9dc4-8517206ed234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 46
        },
        {
            "id": "229a9b52-6462-48e6-96aa-b2401a80f75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 65
        },
        {
            "id": "35b92c2e-edef-488e-9b27-72238d6199cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "34e0e78d-ea2a-4910-97e8-0437c38fb73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 88
        },
        {
            "id": "d7b522eb-c553-4472-8841-1bc1de040d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "f455c535-f514-4c81-ba26-6a0726c80981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "c848d624-482c-4a76-bf26-54d4ee3ae182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "0bb38b35-5177-4224-a3a1-d3f7c9d25896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 53
        },
        {
            "id": "592f8928-c95a-4517-b0ac-e10da43c6730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "5dbfc76d-58bb-4af1-b02c-9d3724e3171c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 74
        },
        {
            "id": "cd15c2e0-ac03-46fc-a9c2-ef02da8885db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "e2f4067d-6631-4d1f-b51f-feac1f147dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "c139c723-691d-49e1-b94b-577eca9ebc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 44
        },
        {
            "id": "bac234b9-5896-402c-9009-f0d28fda5f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 46
        },
        {
            "id": "db413b10-62de-4ee5-ae60-e2cd7747906e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "bbc9e26e-e3ac-4133-bc3e-2cab8b652043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 49
        },
        {
            "id": "76183acc-97d5-4718-b9f5-44aa51a7d98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "3f727e4a-04e2-4571-ad0b-a34eaecc672a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 52
        },
        {
            "id": "472590df-6891-49a7-ade7-3200a02f72f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "46223fd2-c9fd-4d96-846a-4872ab190fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "da8b1b1c-9bda-445e-9e1c-b63e1041db95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "e97f3cd3-460e-403d-a3f7-e3da7596834b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "dc545954-3c88-45b3-8fbd-76cd437fe577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "4b67b782-3451-41b8-8ed0-404fe4d5a494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "c3eb32c6-a10e-4e79-8b64-8de2fdf12cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 63
        },
        {
            "id": "58393cc5-819f-468d-aa78-e0fdc4a82f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "bc2197e2-c679-4d9a-a690-431c193e2803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "accce44d-7c41-48e3-ba8f-7cca821cd7a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 74
        },
        {
            "id": "aa934a79-703d-4f12-8fea-9d213a7b783d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "27421382-16bf-4143-a784-56d0c4f4b8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "50c3f514-b948-44b5-8750-f6ae77b617ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 83
        },
        {
            "id": "ec47c820-5fcb-4c93-88b6-a4e59e32b9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "cdf14584-a3a3-41b4-982b-d1c0fa4e44c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 34
        },
        {
            "id": "64eb0555-2ca7-4ed8-8748-f08daa57ddaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 39
        },
        {
            "id": "1332b8c7-1c3f-4128-a542-ff7f6d7c8330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 49
        },
        {
            "id": "1279b418-a4c7-4f04-b283-832a54c1600d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 52
        },
        {
            "id": "4da050e3-8e85-4642-a8dd-44a3d2da02c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "5d6c8d59-377d-463b-b367-d41f70877e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 63
        },
        {
            "id": "6e6af7c9-2bdb-4b63-9baf-a6375cac48f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 73
        },
        {
            "id": "abd5dd0b-cbee-480e-b41e-0e137a9fdb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "41d6e1a8-b19e-4214-86fe-e57cbc4c4cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "56c028b2-dd43-4783-b377-158a95d54d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "18af8e8c-a0d1-4474-ae00-442b6afec957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "2fcdedc6-f174-4d15-a39d-03d52b26a477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "61d970fd-9144-4f78-bdea-d4e07f480d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 69
        },
        {
            "id": "bcb7f5ea-4b51-4fa3-8882-91b6f363f913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 78
        },
        {
            "id": "a6a576c4-0c86-4fcd-8ef5-c065e4e19510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "6e29e934-055d-428a-9e11-18ab5059c786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 34
        },
        {
            "id": "55f9e3e3-0bb1-477e-bda1-00f5602b0fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 39
        },
        {
            "id": "ada5a042-f62e-44ad-b762-a4f9fabdbf5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "ca5a4d38-fe18-468a-8ee6-f1f7cca7dd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "833bdfec-69f9-492c-9d2c-05154d791c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "4d01cfc1-50f3-4048-8586-db44ee23abcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 63
        },
        {
            "id": "1aa2577a-b700-478e-8b04-bc3d0a0c16cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "94257edc-cb0b-4e7f-b6c4-cd249e915678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 72
        },
        {
            "id": "258eb169-6b04-4b79-a83d-af2273b175c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 73
        },
        {
            "id": "41370e5e-c2c3-4945-a232-2ad71806958e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 78
        },
        {
            "id": "15882730-0dd1-40cd-bafb-8485f58b989f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 84
        },
        {
            "id": "b171bb24-eec5-4065-8d4b-562f2d4ec2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 85
        },
        {
            "id": "ec9ce2ca-e4ee-41b1-a176-a8f31f1b8dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "a0675538-4e60-42a4-a044-d65066b53315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 87
        },
        {
            "id": "870016e1-d1bf-4eac-b405-2e256ac78683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 88
        },
        {
            "id": "4123116e-5b4b-4456-af14-2c2bb3dc4ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 89
        },
        {
            "id": "857597f5-cc1d-4cb7-b53d-6477b4d7648a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "c736aae5-2b74-4545-af6a-57c96fe472c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "858af5f4-c8b9-4d4b-a8c3-758816fb0215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 53
        },
        {
            "id": "63015bc3-1d9d-43f3-99c6-664a50cc6ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "dfe6bf31-6091-4e3d-9e0f-2f8b7d2dd6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 72
        },
        {
            "id": "ef543a52-99f4-4771-9671-52ceb122801b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 74
        },
        {
            "id": "9815fda3-5faa-4d78-89ad-cdafc6baff25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 83
        },
        {
            "id": "3a61efae-f2fc-4cae-a663-4fa4dc9829aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 86
        },
        {
            "id": "d9edc260-cf09-4505-b80b-add656494887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 88
        },
        {
            "id": "993c02f9-9137-4a8a-bfce-ba3d5fa94d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 89
        },
        {
            "id": "02ac2762-7690-46c5-9399-4df901b56fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "fa65d388-8711-4fee-8f71-4fac7fb50fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 115
        },
        {
            "id": "97f36e0b-0e4b-499e-8b38-198cd01edcc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "624670c1-3d45-498b-91b2-061bdc01cc68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 34
        },
        {
            "id": "07fcb4c4-74ca-46bc-939f-096d2854a61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 39
        },
        {
            "id": "df97f285-3e0f-49f8-b122-c8db4df3d8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "bfce027c-6fd3-4339-b26e-82bf0a8b8d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "fbb6e8ca-be5c-4027-b33c-dd1c8541ca96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 48
        },
        {
            "id": "e807b42c-6c90-4e00-81e4-d740e82108ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 49
        },
        {
            "id": "a587cfe8-5049-45f4-9625-e59cfe4b9a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 51
        },
        {
            "id": "426bfa01-cb41-4c45-9a09-ef14ed544e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 52
        },
        {
            "id": "fe52fd2c-ad18-4f97-bb37-d4dcd7590742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "6608b65b-fc79-404b-8535-37ac1f187d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 54
        },
        {
            "id": "eedbb4ff-9e0f-42b7-9c7e-7d87e06492bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "51448299-8f0e-406e-9fa5-e999defe5c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 56
        },
        {
            "id": "28758f8c-fda5-4b10-ae9a-ac7b05faf48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 57
        },
        {
            "id": "922e13e3-7eb4-4c14-9a04-30c1bd97cd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 58
        },
        {
            "id": "5a7f99bb-dc28-42a4-9cfb-f9b6ed96da37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "3b2affbd-8140-41c0-8610-5f5b912be456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 63
        },
        {
            "id": "9d26234f-05d8-410a-afa2-b599f140fd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 67
        },
        {
            "id": "57964b50-fd21-4abf-adb0-7a2710928107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 71
        },
        {
            "id": "294d37ff-6632-41e1-bf3b-2f2a7c30ce50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 74
        },
        {
            "id": "5308994c-1a16-4ffc-a95e-bc9bc31b43fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 79
        },
        {
            "id": "584da959-2355-4b4a-8553-9f7af2fe884c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 81
        },
        {
            "id": "28defca2-52d2-43ca-9e8a-8419fd4d1e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 83
        },
        {
            "id": "36443443-7c44-401d-a730-113bded8496f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 84
        },
        {
            "id": "a1a32a27-c970-47a2-82b0-db0af67bb521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 85
        },
        {
            "id": "9e70e7b0-28c4-4a9c-8c2e-af13a51a3eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 86
        },
        {
            "id": "8baed397-645f-4dba-8edb-3786a8d7cfb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 87
        },
        {
            "id": "288a1d2c-1f02-43cd-aa9d-bdbc67ace1e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 89
        },
        {
            "id": "18340f2e-b032-4cf6-8021-d85fe4f24aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "dee17ed5-5c78-4d6d-b3ac-758fba03a65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 44
        },
        {
            "id": "aabbc57c-6dbe-47ad-9a2d-fef26d55edb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 50
        },
        {
            "id": "e5b594bb-d8e1-4dc2-803f-91b89422c125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 52
        },
        {
            "id": "44bd1cfa-ac6a-4e32-95ae-2e07d524cfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 65
        },
        {
            "id": "15045bab-68a3-470e-9bb7-6836f4c369ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 66
        },
        {
            "id": "9cd6c8ec-6fb5-4b9e-8f74-d1ecb26b389b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 68
        },
        {
            "id": "7a98ca7a-a79f-4eee-9ac0-e1b7ac35c93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 70
        },
        {
            "id": "759b2e85-dab9-4acd-b8f4-11e7374abccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "098771ad-e0d8-428a-be06-70f8cab55b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 72
        },
        {
            "id": "21542eaf-a212-4cd5-9c5a-43fb70d2565b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 73
        },
        {
            "id": "b1da443e-cf68-46d1-bd8d-484bf4d6ee0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 74
        },
        {
            "id": "aaa531de-d9ac-4cde-9bcb-ce07436b9edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 75
        },
        {
            "id": "d64eda5c-1d01-452b-939a-40ecfd0db487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 76
        },
        {
            "id": "b0a54d11-1c1f-4523-a779-bbcd96ae7423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 77
        },
        {
            "id": "d09b1384-9a17-45a9-a02e-e566c2369c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 78
        },
        {
            "id": "3f38aeea-9ec4-49ee-b124-08e81e311944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 80
        },
        {
            "id": "d4338aba-658d-4fba-824b-535d569c56c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 82
        },
        {
            "id": "cbdb17be-82e1-4db9-831e-23b3ca046338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 83
        },
        {
            "id": "1babf428-5adb-442b-b579-b8d718371461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "1bfa12d0-29af-412c-935a-69ceb959587a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "b9affd25-fb86-4e30-a026-70ba34cd3b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 88
        },
        {
            "id": "ff4d46e6-88e7-4664-8648-88c111a3c0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "56387963-2b2b-4349-b5bd-402da0554171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 90
        },
        {
            "id": "da4908bf-464e-47bb-afdb-d9cadd22948a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "21a874a0-c31a-44a4-a66d-9cfddeda64b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "ebf51554-f148-4d4b-951f-73abc8887a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "316dfce9-0f79-47fd-aa99-975f09b0863b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "8342b198-7810-4aea-b7da-25a50c20b544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 88
        },
        {
            "id": "e00654df-244e-4c8b-b1f4-080042ea7287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 89
        },
        {
            "id": "178ff434-2175-498f-bd0e-4ce7fa727b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 34
        },
        {
            "id": "cffded09-b20d-4db2-a952-344d7815343d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 39
        },
        {
            "id": "5b9df1c9-881d-468f-976c-a16daf48395d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "b0178d48-d46a-4c1a-8ae1-62e9e44bc4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "3c00fecb-b320-4d77-870d-184073ab5f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 52
        },
        {
            "id": "b8a50977-78cf-443a-8810-f5b23c8d4067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 53
        },
        {
            "id": "d559d4bc-fc65-4ee2-a69b-5795d888fe31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 59
        },
        {
            "id": "b4e77d45-d525-43c3-a13b-58b8a9737329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "120cc8d2-2ac4-4e4f-8ae9-999ee827eed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 74
        },
        {
            "id": "40d03bdc-9e31-4374-879d-bd7d402fb82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 83
        },
        {
            "id": "161d9298-dbfc-4089-8ef1-aafc8dfb7676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 88
        },
        {
            "id": "c45d5157-0fb6-4941-af8c-b27d97730c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "201f0c7d-7f72-489c-9b09-b2e5a45fd996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 114
        },
        {
            "id": "821a49d5-8dd1-4db2-aa30-00248b014712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "eddba14e-2b2c-40f3-a76f-a58f900c6146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "69826076-f1da-43d7-ad81-ab6474caf0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "feadf96c-5f95-4cb5-b975-1b191e0c54d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "d898c4d6-9210-4002-acf8-3364e92d384b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "5c569b22-faf0-4610-891d-09452312c161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "0e8e090b-df30-49ff-b949-df980c08615f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "fa6efef9-ffde-44a8-935f-d2da928e15eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 39
        },
        {
            "id": "aafbe801-dd5b-4a46-b7ad-1b4bdcb9331e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "dc7c5586-cc62-48fb-bf7a-02d2deb62add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "904f154e-bfd4-47ff-b453-d3ecf1561c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 48
        },
        {
            "id": "950072d8-8180-4e7e-a990-ee7a4af6d0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "88cafe95-ef61-47d5-9bb6-e5e3a9ac987c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "4ef35333-e8ef-499f-adf1-44484a41e5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "9863d8b2-24e9-41e4-b8f1-fec295aed797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 52
        },
        {
            "id": "05d2012c-e175-4326-84ef-29cc87bd61e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 53
        },
        {
            "id": "e70d4a88-6d68-4d5b-addb-4c8203d31402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 54
        },
        {
            "id": "2aee042c-b652-4a56-8027-dbff2cc860c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "e2da6276-88f8-4262-b5da-c96d3ed6cd96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "ad81c122-4b8d-469d-8556-0328c4c7ed72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "107a7818-18b8-43fe-a5df-da5db8abcb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "cc695e11-7b80-4d8f-a8db-0519faa3df86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "ee4d5b51-3b2b-4002-af23-e5c6a0580280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "6845dec9-8da0-4cfd-be96-6d8198191e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 74
        },
        {
            "id": "3b5a8f7a-0bee-4d2e-bca5-98dc9427c966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "06a1322c-333e-44a3-b343-090134791013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "c3b50c5d-f989-4522-87da-43017e1dc7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 83
        },
        {
            "id": "8261d5af-6a64-4933-841e-f53d612236d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 88
        },
        {
            "id": "abbc2655-9276-411f-b346-7bb42adf3798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "32b222a7-a9ad-4376-88ba-6fa594a3caf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 114
        },
        {
            "id": "94636be3-1a02-44b2-98e2-c2a2d9416c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "e962db2f-b3ac-4799-8def-f8a1a3622ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 39
        },
        {
            "id": "f0fbe16f-1813-4eda-9d1e-946a00782714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "6bbbd2b7-91ca-4412-9362-f18d1e65b2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "91d8e4e1-e20f-4c4f-bcff-2292388843d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 48
        },
        {
            "id": "5239acc6-c20c-48f3-96fe-c632a1a56f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 49
        },
        {
            "id": "aef8cc69-eed2-4f87-b1bd-60cb02d92af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "0d53909a-7026-4380-b431-dd15fdc7d54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "8127161e-2671-47b9-ae9e-88f3ce84ebc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 52
        },
        {
            "id": "a01cb71a-0bcc-4c79-a11e-b6341a452980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 53
        },
        {
            "id": "37a08a54-253c-4d4e-ae67-f0c5c38350ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 54
        },
        {
            "id": "a000d607-6398-43e7-8f1b-f2a509294c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "1feff54c-52bd-44f9-8ad8-1b30aa88e3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 57
        },
        {
            "id": "04c9b2d2-99c0-41fd-a97e-1d3eae2f12d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "d256be23-53d6-4d4f-af4f-876c09ce9326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "12f2d02e-9a9d-4091-ab3b-fcb4aa08b64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "df0fe784-f5b6-4937-8e8e-f1a0df862224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "74e4a8ab-4745-431a-acec-574cebfbd530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 74
        },
        {
            "id": "16f61eea-9243-4d16-a62a-86138fae0c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 81
        },
        {
            "id": "41ea6f94-936e-4cf9-b814-3bc879c03c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 83
        },
        {
            "id": "36e4aa39-e5a2-49ae-b7ba-056c404264f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "be361385-e2fb-4d1d-ae7c-ad039cc410ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 114
        },
        {
            "id": "e80f7526-676d-4a07-bd28-b4734df4faa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 46
        },
        {
            "id": "3c06d30a-5d95-4de6-a3e1-7c99a13e7c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "5059d135-a857-47e8-8edb-0f26beef9eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 49
        },
        {
            "id": "671a77ae-7ac1-483a-846e-b60ca6b3da00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 51
        },
        {
            "id": "604ca9e3-f2f7-40a1-926f-491785f54260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 52
        },
        {
            "id": "17972750-04ac-4fab-a13b-5c2c8e0705ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "4f7cd952-0a23-4f93-8309-040a6b6ade85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "2c721d55-8027-42d4-a014-a283e3fd675d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "54f25e86-c83c-4319-8f05-fd9b10e993ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "b57e5a8c-cebf-4662-8aed-a2657b96b63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "83e2b38c-1aca-4795-9437-7b82edad313a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "c8d55f79-19aa-47cd-8685-585d3d768393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 63
        },
        {
            "id": "76f3bb4b-3a7b-462b-a9c4-17873c4f8e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "981aab8f-3b40-4d9b-9be6-c3bb0a6585fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 71
        },
        {
            "id": "2f2714ac-d21f-4dc0-b4ef-decf2f1ac751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 73
        },
        {
            "id": "b650da91-2fc3-49c7-ae8d-9dec3c4e0123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 74
        },
        {
            "id": "c5e3e02b-788b-406b-a88e-964ee67a3cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "d7ca42b7-bee0-4513-8d7e-7f6c87b64ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "48c134c4-b783-4282-a280-6a569e2b8a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 83
        },
        {
            "id": "f754b513-af70-43b7-a66a-5b92dc7ca750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "e678dd31-3900-44aa-9800-759e61203f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "6555bb4f-6f4c-49b8-858e-e08365bfd3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 48
        },
        {
            "id": "f0effed6-3a65-462f-b175-9e101a9fdf3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 49
        },
        {
            "id": "98fcc83a-6358-47b7-ad08-4644bad54c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 50
        },
        {
            "id": "5b63a8cf-9a69-492b-8eb2-166372820dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 51
        },
        {
            "id": "94db53d5-4e08-464a-812c-d00dbdb6545e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 52
        },
        {
            "id": "76eb8142-637f-43c7-881d-3a78f6338d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 53
        },
        {
            "id": "e7a474b9-45fd-4aad-9517-f93d7711c786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 54
        },
        {
            "id": "dd5aa7a7-b4f1-4bfb-8983-09084dda1fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 56
        },
        {
            "id": "9d9198a1-ac80-4e93-8acf-7ca4667c994c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "85ca551e-99ef-4594-9754-83f73f439636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "7207ac8e-a695-4364-9649-bde96657b547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 63
        },
        {
            "id": "77349c12-1bc0-4c80-b981-de9af6028a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "6d061fe2-74fc-4964-bbf7-18be14eebb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 67
        },
        {
            "id": "3ed9ae06-a40d-424e-88b0-c5eb63adb757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 71
        },
        {
            "id": "d8899b81-d7e2-4bf0-b755-7d1482c5ee01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 74
        },
        {
            "id": "6a25d2f7-d444-436d-9d6d-f22773eaf38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 79
        },
        {
            "id": "4d5e8ecf-5141-4e11-aa46-128f2e19a0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "b88aebdd-8edb-4dfc-8802-15a41cc63db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 83
        },
        {
            "id": "ddac7fd6-fca1-4f65-9649-e110fbde9159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "fa7afbc9-096b-40c2-add9-0df845a17eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 89
        },
        {
            "id": "c89599a2-f8b3-4409-92b5-446b3563c910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "5bd6ff22-4bc5-4463-a421-cc89b2788812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "1cfcd7f1-95f8-4091-bf9f-30d66f322f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 44
        },
        {
            "id": "4442abd1-0541-4ea9-9bf7-208886d90e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 46
        },
        {
            "id": "e708d89b-2f83-457c-adcc-e817c0ceb398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 52
        },
        {
            "id": "292469de-229d-490b-ac2d-bffb7a46a961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 53
        },
        {
            "id": "24a73395-8819-42d6-a553-87ac2b883184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 65
        },
        {
            "id": "4aed969b-7b64-4725-8cd4-e080df5a3691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "7577e8dc-704c-417f-98df-8d30bf0fea79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 74
        },
        {
            "id": "ec9ea13d-4d3e-4d19-a9f4-73b55a194ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 83
        },
        {
            "id": "612b1871-fc11-4e4c-b3e3-8d49b9515c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 88
        },
        {
            "id": "c597ed73-1d90-4d06-b001-d72423b2067f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "0f0dab3a-004b-42bc-b1c8-acf8fe4ccd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 99
        },
        {
            "id": "6a261c89-c221-4ef6-a884-453851b5cac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 101
        },
        {
            "id": "0e742e66-94a3-43ea-ba70-6c307e0dc770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 102
        },
        {
            "id": "bb3d389a-f704-44b9-9423-c01ffc537123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "4d808319-7a2f-417e-a1a0-1f68150b6a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "5b56a1cd-0c10-4977-9b0f-e752579e6cba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "ea875901-c1df-48bf-8407-4d281d352ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "4e102d6b-644a-4fd8-b75f-3cdb07d267e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "8b9775ee-c7ee-4208-b695-7f8e16f0dec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "90c2eea4-42a1-4ac2-ac95-9de3a644e0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 111
        },
        {
            "id": "ffb5c1c0-6f22-4e70-a090-dabf7657a6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "ad36d25e-df25-4028-99dc-16536b3f3ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 100
        },
        {
            "id": "1c23fa4f-d1dd-4239-a113-7ad8b9bcaafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "e8b6ea34-3b31-41b2-a0f9-7c41e2153993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "49e0c84a-2921-4c99-969b-2b2d347947ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "3d498276-d710-4a22-9018-b5f36e885d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "998aec76-493c-4356-91ed-a675a02a6ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "a7ac903b-a382-409c-b8df-57d1f146d0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "b71f26be-9a15-40cf-930a-5cae194abfdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "028c1158-572d-4948-b42d-23d84e0bd069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Kooky",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}