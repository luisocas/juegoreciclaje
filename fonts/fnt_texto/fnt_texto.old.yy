{
    "id": "08cad629-f4a9-495c-96cd-7fb58d4f922d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_texto",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cartoonist",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "645ffdce-2cb3-4380-9e9f-8272beb4b75e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b1c23252-d64d-4022-8695-470e31b6e905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 143,
                "y": 146
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f74484a6-c6dc-4db0-9e1a-e364ffe44f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 126,
                "y": 146
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6bd40b61-f99e-48a7-a824-0f65fcb9c704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 124,
                "y": 146
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b2d65065-98bf-4aee-bd55-f0d1ea7e1b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 122,
                "y": 146
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "989c7e23-c61a-4db8-94df-afa859d94fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 120,
                "y": 146
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8ee4125e-7355-497c-b0cc-9d98daf1e2b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 118,
                "y": 146
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "bad9f1dd-291d-4601-b21f-45fd6102caff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 146
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cfe13477-1c0c-4fa1-bf64-13111a6bfe9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 107,
                "y": 146
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "088848d5-6a5d-4984-ba51-6c2996dd5f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 105,
                "y": 146
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bc7b24cd-93e3-474c-9d8f-678901402a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 154,
                "y": 146
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "33c77995-e5bd-4ba7-b930-3eca29a7148f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 103,
                "y": 146
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c4dc5f20-3bfb-403d-b3c4-292ffeeeadbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 71,
                "y": 146
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "35a5b4e0-423e-4e4b-bd12-0e17aa7d0cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 69,
                "y": 146
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ab230f78-b12e-4b96-b739-318e6bb05294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 56,
                "y": 146
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d84b1702-3106-405e-98f3-aa5a05b24df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 54,
                "y": 146
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2285e46e-6d0c-478f-bc68-c1788056d2f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 33,
                "y": 146
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fa71e247-ff3d-4fbb-a105-bead6fb31a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 20,
                "y": 146
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ebcc3398-e897-4ce4-8b82-4aed60807a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b4ed6114-79f6-4a53-94a3-0f3d276a04ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 220,
                "y": 110
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6b79b613-b50f-4a56-8bce-1281457bbb57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 200,
                "y": 110
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f4b75de6-8173-436f-9e91-abd0a0a0f00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 83,
                "y": 146
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3979fd44-c98b-48e7-8baf-704d888ea400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 156,
                "y": 146
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d193396c-ff52-4f01-ad0b-3b18a8c75d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 175,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d16e8fe8-bdcd-445e-b570-3be01b2d354d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 194,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6a0fe6ff-4b2c-4aec-b42d-ad6a3e487a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 16,
                "y": 218
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "74791cbb-3152-47be-a570-6fb8ee202b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9e57e8f5-d05f-4fc6-a045-f9a412330fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "66535804-bf8d-45e8-8ef7-3ac24d56d4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 235,
                "y": 182
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9aa80e77-83e1-42d2-94e2-e918f3cd657c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 233,
                "y": 182
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "672be7e2-1433-42c6-88ab-d3a1865b38ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 231,
                "y": 182
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ea1f5e64-50d9-4957-9997-eda29c02cbcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 212,
                "y": 182
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d15349a8-1b4b-438b-83dc-4ab650b468e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 210,
                "y": 182
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "859ab2c3-8f9c-4f4b-9a1e-43984547ea3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 187,
                "y": 182
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a94d15e1-592f-404b-aacc-3a319a238674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 168,
                "y": 182
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "029ce0d0-a0fd-4bd8-8ee9-8779389fad99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 148,
                "y": 182
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8c1497fc-c7fd-41a2-83bd-0402c91ff5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 129,
                "y": 182
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b1baf4b7-ada4-43fe-bc1e-b43473bc778d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 112,
                "y": 182
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2ae97d82-a1a0-4360-880a-417c56b72d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 96,
                "y": 182
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "388ce498-b96b-4547-9c75-9687544f3d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 74,
                "y": 182
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6f6e5aea-4597-4d6c-b091-08210c7d4f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 52,
                "y": 182
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a91d133e-edcf-4f11-b045-e2d170e530b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 41,
                "y": 182
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6d9790b4-c823-4726-966e-2a10d45fce12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 21,
                "y": 182
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bf0b5f03-45a9-40c2-915e-1f63fc3d4298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dec403cf-f4cb-4478-8005-367bc161d3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a221d1ef-c2f5-4f8a-a3eb-b49e85f41f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 212,
                "y": 146
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a36812b7-fb5f-4d5d-8f34-6346192b915a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 180,
                "y": 110
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5efd0a66-f613-4b4a-90a3-18757b240175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 159,
                "y": 110
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0dd897db-a7d4-4764-917a-4f95ee9c6160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 141,
                "y": 110
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "077a28e7-df7e-43e3-8aa2-076b6d245ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 159,
                "y": 38
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "da4afea9-8758-4d87-9a18-37b80771de1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 137,
                "y": 38
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "02e0fc40-2478-4610-a7f6-91ed3a3cf764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 117,
                "y": 38
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2a3a83dc-481c-4e0f-9bd1-5020ba05d0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 97,
                "y": 38
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bb5fd377-4253-4d99-a900-cec2336a1399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 77,
                "y": 38
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "02808a75-fc6f-4440-89c7-7cb3bdc0f883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8425921d-1c3f-4949-acdf-bb7d468ec19c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 26,
                "y": 38
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "78533856-ea27-4dd4-8ba9-3af2046fd51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fe4cca90-9cbb-446a-97b2-676be9b98143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0b7bebda-eef0-498d-8cd1-aeb42504151b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "99714cd0-8e36-422a-96ed-8360c0aee9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 157,
                "y": 38
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6d1f4332-d922-48f9-9382-6ded3fa86a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b74a9f65-6a68-4b90-85eb-d65f9830b500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 4,
                "shift": 19,
                "w": 9,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c3ae634d-09ff-4003-bdc5-7b0f499994d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2280e16b-e0f2-4748-b6f3-1c869aef2b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2778db70-5906-44ba-b3db-f4c5ee906faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b1c357f9-8f71-468a-a36c-96c172cdb945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cc3f129e-24b6-4e90-9c0d-ef2b02deb86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7046aebf-54dd-491e-aeea-2803a3864c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "61318acc-1126-4113-b305-d31c1e13d195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "13603b8e-e38d-4fa9-bb6d-447baaebf1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0dabbec4-ed8e-4c70-b083-dd23a025bce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "21ea435f-28d7-4cf6-833b-7012de7a62fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 183,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c5c4eeed-3659-40f4-b206-9d8837e9b21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 139,
                "y": 74
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "91343bc8-ef92-47ca-ba1a-b7320ea1bbc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 205,
                "y": 38
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3b05abac-e79e-40d8-a033-d379c90124e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a7ec4c5d-74e0-47c8-ae10-7dd51941731f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 82,
                "y": 110
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e723a5ce-6997-4f41-bacb-4655ff768229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a1ac6464-d0ea-4d11-9107-3f43ce7a4fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 43,
                "y": 110
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "933e110c-6af9-4ccd-a906-a87b7a69c798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 23,
                "y": 110
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aa43f430-28e5-43b3-bdbd-de0ffbf3acd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e5fb8014-cf3a-4e74-85e3-2c59333bbdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 225,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a0f6bea3-75cc-4321-b4f2-a133cc37d35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d3f5486f-6983-4c3c-b0db-91433f9c4d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "80e04ab1-d83b-4ded-9486-2a311330f187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 121,
                "y": 110
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "29ea8e3f-52c0-4a41-b01d-a9b28b94f065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 161,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "475dcb77-5174-42cd-83d8-d87cb1ca2f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "36238012-c1d7-4c77-9732-f4bb92a5a62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 98,
                "y": 74
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5f8a62e2-814f-4b0c-b9e6-1030245604fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "19264ebe-ea68-4c02-ba0a-48bbfdc81252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 44,
                "y": 74
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "58c0d7c4-1982-4414-8c6e-deb2ba8a4a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "78681664-1b41-4d75-9014-38ffb0fc7ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "72499891-34c5-458d-84c5-75593bdeaefb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 236,
                "y": 38
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ae77441a-ed72-4e61-8fee-7c858535ff07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 231,
                "y": 38
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8a356a0b-b04d-46ea-8459-cc4b0fb13411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 216,
                "y": 38
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c51ea188-63cc-48d4-84cd-1c5502e8e7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 35,
                "y": 218
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "6510b7a4-cc4f-487a-9f41-61ece17c60f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 34,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 52,
                "y": 218
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b14b9a34-9ffa-4e56-a14c-e016029b4ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 44
        },
        {
            "id": "9b0af27a-c775-45a4-959b-2d225885a3a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 46
        },
        {
            "id": "ae732055-a774-4721-a0b9-bf0e419e524d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "a1251a0e-47f8-4717-a78e-81edc4a3b7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 56
        },
        {
            "id": "a171e9d4-0326-4cc9-908f-4e02e3803d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 63
        },
        {
            "id": "baf3cc1e-70f4-42c1-8dfc-882a3c361a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 65
        },
        {
            "id": "f1533468-558e-4b66-94ac-468edbedda73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 33,
            "second": 74
        },
        {
            "id": "c7e7007c-1ba4-41bc-a44e-345e074450b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 83
        },
        {
            "id": "7517bd23-f23a-4a9a-9318-7632c35d8553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 88
        },
        {
            "id": "22d13123-1f44-49e4-b116-668e2d401a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 34,
            "second": 44
        },
        {
            "id": "fd6f8458-5133-4a6d-a2b1-9a41bc0efef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 34,
            "second": 46
        },
        {
            "id": "96a2df30-99b0-47cf-848a-c74fe981f833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 48
        },
        {
            "id": "4b675b2d-397c-4184-b3f7-da9aa8bfee4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 52
        },
        {
            "id": "b67d6aa5-0173-4e73-a0c3-dc00057c238a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 53
        },
        {
            "id": "f83bcb3f-6fd9-434f-a5c7-d42597b2d67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 54
        },
        {
            "id": "482481e9-8a8c-40ab-bcab-b17366160a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "dd41fe66-0436-452a-8fa4-442677adf106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 65
        },
        {
            "id": "5c09bb47-19ca-428e-97c7-5e0d56fe0bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 67
        },
        {
            "id": "b50b0c34-ac4a-4cd8-85c2-948f41a51ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 71
        },
        {
            "id": "fa3ce9ec-e66c-459e-b5e2-5b02a7bbcb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 74
        },
        {
            "id": "9939ac6f-e641-49e1-876a-e2d7a0e58d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 79
        },
        {
            "id": "cdca0710-b22c-4b2a-bc97-c6511b317303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 81
        },
        {
            "id": "a1ad2901-97b5-4854-8ad0-292f81c923a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 83
        },
        {
            "id": "c6c4de2e-2361-45c8-b792-fedcddca9eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 44
        },
        {
            "id": "cb403888-2457-4829-b2e3-01755221eb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 46
        },
        {
            "id": "7d7f48df-12b4-4de2-a9fb-c2ab5647dd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 48
        },
        {
            "id": "6ba289d7-b322-4bf7-93c6-69347ac13a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 52
        },
        {
            "id": "e9cc2e68-328b-4569-b566-131bafafbd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 53
        },
        {
            "id": "be49d807-2b64-4ba9-96d0-15c1430fb392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 54
        },
        {
            "id": "756a91a6-b851-49a5-8980-548e44a82c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "b51b220a-1726-4a2b-b17d-4a34850c4dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 65
        },
        {
            "id": "bc1d6754-faf9-48c7-89fc-1074a359ace0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 67
        },
        {
            "id": "81f7e985-a5f5-4b7d-97af-0694415111e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 71
        },
        {
            "id": "4276a4e9-3cd3-4ba8-820e-7b51f4ab4627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 39,
            "second": 74
        },
        {
            "id": "8c12af7c-0dc6-436d-a696-57a73a72f608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 79
        },
        {
            "id": "de57b9ee-df0a-4d10-96b5-080a9fba4c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 81
        },
        {
            "id": "346558fb-e6ab-45c3-8bcc-599b20228515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 83
        },
        {
            "id": "4d61bff7-61c8-4b2a-8615-8dc1067220a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 39,
            "second": 106
        },
        {
            "id": "053e2bde-2322-455a-b07f-a3e2e3adfe75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "d350a84a-a00d-4056-a650-9a2be23036ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 44,
            "second": 34
        },
        {
            "id": "b39c42fd-e006-4028-a715-2db451f531ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 44,
            "second": 39
        },
        {
            "id": "413ffc0e-fca4-4a05-b5e5-3f4e28abafdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 49
        },
        {
            "id": "76ba84f7-7b99-4bb8-bc73-a6ea7ef71076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 52
        },
        {
            "id": "5e834dbd-03d0-43ad-aac4-438586b18469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 55
        },
        {
            "id": "502a2950-181e-42f3-a9f1-9959b0956e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "fc587a11-7f58-48ce-b5f9-d157d64b9ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 63
        },
        {
            "id": "9e6f13ef-16ce-4358-ba12-17db67cb88bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "c796d84d-14da-4fab-886d-138729e7c2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 72
        },
        {
            "id": "37ba9edc-32b6-4443-9680-17cfb141b686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 73
        },
        {
            "id": "94a48dbe-8ca1-4776-81b9-72fc21de773b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "f1522d58-36f8-490c-bf30-b6633b29ad88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "53f139e1-3bd3-43a7-8be3-c7b298f8bb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 86
        },
        {
            "id": "ded451d5-34f5-4aed-8ccf-d951354c79b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 87
        },
        {
            "id": "25bbe1a0-34b6-44e5-b76e-6dd7764ab938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 89
        },
        {
            "id": "1333e8f0-b6ac-4d76-8865-4df897ded333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 90
        },
        {
            "id": "2ecfab90-4092-437f-9ddf-fdbf3f214199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 46,
            "second": 34
        },
        {
            "id": "0306957f-41d8-485a-b8d7-6e9ff96a548e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 46,
            "second": 39
        },
        {
            "id": "833a51b0-adf0-4f66-87fe-31176b45532e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 49
        },
        {
            "id": "b64d80b4-1ceb-4421-994e-d8c04cfbcbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 52
        },
        {
            "id": "3b809da2-7e9b-4091-b19c-fa8ea29a42f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 55
        },
        {
            "id": "54fde4cf-d586-45a1-8b80-cfa7bd9f9455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "9be8a968-6a40-4f6a-9d0a-d4a122259be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 63
        },
        {
            "id": "7c207e3f-23c0-412b-b949-f3c710b4700d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "e2ced35e-1574-40d8-a8a1-88967ba4961d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "48128051-cc25-4359-b0c5-6b6728c2f985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 86
        },
        {
            "id": "9d457806-a478-4d0d-92d6-247f12a11e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 87
        },
        {
            "id": "5a21c7a2-7b91-41c2-9f62-1ac2bf452078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 88
        },
        {
            "id": "2304cf85-efee-4e21-8368-2764cfda79af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 89
        },
        {
            "id": "d037a2a3-0b82-4684-ab85-2eb9b9a7289b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 34
        },
        {
            "id": "e6687656-9433-41d9-aea8-8184f983de23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 39
        },
        {
            "id": "50870f58-1ea9-43f8-97b6-2c4ea0949c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 46
        },
        {
            "id": "db538070-f8a0-4b14-9bf3-2182e03ae086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 49
        },
        {
            "id": "8a163de3-c26d-4d1e-aa86-94cd86977b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 58
        },
        {
            "id": "1f7c5f3a-0a10-4b0a-8e81-c05f3a9bf566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 63
        },
        {
            "id": "db347684-fe43-4330-9aeb-a981f299de8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 65
        },
        {
            "id": "29762607-33ea-4da5-9f63-bc6e1a563239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 72
        },
        {
            "id": "07b17fe1-f69b-41c1-9aaa-5830d6504bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 73
        },
        {
            "id": "287f1c35-a595-4a56-aba8-1cb3f0fe9adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 84
        },
        {
            "id": "a580be8a-2f27-4469-a5fb-14dd714fa21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 86
        },
        {
            "id": "3a7b61a6-caa9-4d0a-a023-f40f8e94fe8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 87
        },
        {
            "id": "f0f3eb5f-486c-4960-8fd2-82cb4bc13a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 88
        },
        {
            "id": "522f68bb-c6ce-4f7d-b778-8530594ace3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 89
        },
        {
            "id": "6b8b5ece-c75f-4e4f-a8ad-641f8b15f4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 50
        },
        {
            "id": "d7027f80-bc25-4a7f-a081-36feb99966b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "7928063f-01f2-4265-902a-51f6ffc6ae0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 44
        },
        {
            "id": "d46c9b3c-66fd-4b5d-859d-8ea148c5e6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 46
        },
        {
            "id": "b4e9168d-1c0b-4020-a874-cc6ef70580a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 74
        },
        {
            "id": "c7bda0dd-e761-400c-b819-363da4195afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "dcd5e07a-830c-4dc1-898e-62bdf6ed216c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 89
        },
        {
            "id": "6885a306-7b17-4dac-a529-5b5f3b2599a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 52
        },
        {
            "id": "6f451acc-35b3-4ac2-a1a9-7e0c9e57552d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 65
        },
        {
            "id": "49ddbaf9-08ac-4556-9e4c-cd029c49f9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 74
        },
        {
            "id": "8171f900-42b8-4326-bf60-048e0f2ece00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 86
        },
        {
            "id": "23005857-e199-4830-be83-3408b847b19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 88
        },
        {
            "id": "7d65cdbd-2eb6-4299-8d76-f19f44e5bf37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 89
        },
        {
            "id": "d1dbc0fd-4090-4c02-a097-54d64087ff9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 34
        },
        {
            "id": "f30ef844-0c45-4de2-b909-2287759f3278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 39
        },
        {
            "id": "3a422197-f22e-4802-9a78-cdef3ae4a759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 44
        },
        {
            "id": "f2a665be-235d-4e46-ac22-48ed317d1e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 46
        },
        {
            "id": "031dd9e6-be51-42d4-b4c6-52189c366d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 53
        },
        {
            "id": "8611c4ce-c8c0-47d0-a1e2-17adf52825ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 58
        },
        {
            "id": "ec530e0e-2016-4c88-8397-aa115c3141cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 63
        },
        {
            "id": "87c4522e-4827-4d7e-985e-95ee5911bc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "d50a23af-304b-4a60-93c2-0a581438384c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 83
        },
        {
            "id": "4aa387e4-9d77-4668-9fa4-7f4b8b3d42ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 84
        },
        {
            "id": "77fc7454-0cd1-4ea3-9eb8-15b62a5376d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 86
        },
        {
            "id": "1c7f8061-6608-4278-ae2b-e1a94bad8ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 87
        },
        {
            "id": "fd2e3147-a086-405a-b978-cc31e291dc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 88
        },
        {
            "id": "096be317-342a-48fb-bc99-94af7fe58644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 89
        },
        {
            "id": "a336e61b-e9c8-4e01-9551-91dbd61617c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 63
        },
        {
            "id": "2e1ba86d-80f1-4bb9-8e09-2fba8d904d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 65
        },
        {
            "id": "ca0bcd07-85a6-4365-b9c9-4c4f090e324b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 72
        },
        {
            "id": "aaba9be9-07c2-463a-a07d-b27a4482d6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 73
        },
        {
            "id": "440ca975-f0e9-4768-a6da-1d71d2dcf99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 84
        },
        {
            "id": "bf09ee4d-74b1-49a6-8b83-700e28799783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 88
        },
        {
            "id": "a157bd41-f0b5-421c-9a17-02ecd61acacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 89
        },
        {
            "id": "feb92064-ddf8-4fee-aebb-4dead7620077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "27898e7f-643e-45ba-a9ae-290bbbdd184a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "ff9d5476-1b70-417d-ba0d-edee699ca2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "f7454b53-54fc-4531-9871-352c83fe95fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 88
        },
        {
            "id": "5cf926f3-aff7-493f-8c4d-0fa8191fd47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 89
        },
        {
            "id": "752af3e2-9d44-46d4-abdb-4722139b7fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 34
        },
        {
            "id": "216444d7-dba6-4720-870f-202841450657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 39
        },
        {
            "id": "1a47e336-4476-4e15-a405-f0f083094527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 44
        },
        {
            "id": "da234111-1b32-4a17-b0bc-15259661f6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 46
        },
        {
            "id": "c2e6dafd-eac6-4550-9e6e-dcb3b4d4d3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 65
        },
        {
            "id": "9e66903e-06a8-4f31-b162-1507e6640e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 67
        },
        {
            "id": "d180f85d-dd51-4e63-98e1-f6bedf9cd9d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 71
        },
        {
            "id": "12152366-8a1a-4f4b-8c61-3688718910d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 74
        },
        {
            "id": "b8786887-0478-4a77-a75f-030e62383630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 83
        },
        {
            "id": "265b73fa-e98f-44e3-9c43-c54f56292e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 88
        },
        {
            "id": "ab2cfe79-d42f-4f4e-9fcb-508a3a44af84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 57
        },
        {
            "id": "b8270c04-0611-49ec-8f0f-6bc03e01bdc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "c2e0fc7b-9417-409a-8dc5-7d15242ad199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 72
        },
        {
            "id": "b42f824e-fac9-498b-9552-4229ac6d1a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 73
        },
        {
            "id": "6584b7bc-2393-4743-b0c6-f47c9c4386f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "f21a732a-5c63-4c99-badc-a05e4dd40ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 86
        },
        {
            "id": "55b2ce0b-79db-468a-a736-b0a5c27cb1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 87
        },
        {
            "id": "ea674be0-a0bb-451d-9374-fd120a8ce81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 88
        },
        {
            "id": "a9110a58-c27c-40ad-9a9d-7237bcf4db51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 89
        },
        {
            "id": "7ef6226b-d74c-4ceb-9bd7-782dbc045f39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 44
        },
        {
            "id": "a3f091a3-5a76-4bf6-bcdf-7b99efd9cff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 46
        },
        {
            "id": "7651d0c1-b402-40d5-9bf1-b61d314a2961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 65
        },
        {
            "id": "00a0bb39-63b6-4863-a210-7aac0967f7a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 74
        },
        {
            "id": "c0e14569-e1eb-4457-9447-97f7d5a9bae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 83
        },
        {
            "id": "063dff04-1408-4c6a-b9b7-adec071d8db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 86
        },
        {
            "id": "e9ac6202-f607-4ee1-be2f-a7d12a45d561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 88
        },
        {
            "id": "5c2f3533-e048-47b7-bd75-20c88a9f0692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 89
        },
        {
            "id": "27dd5c93-accd-459c-8ad6-1edacee5f00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "6fbc9071-64f0-433d-872f-1768c222299b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 63
        },
        {
            "id": "175c049f-2e8b-4b8f-a76d-ad67479f7c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "5b6dce56-c172-4259-8a5d-c9fc87a64413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "d35229ac-9a1f-42ad-8c3d-335659a9da5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 86
        },
        {
            "id": "3b744233-4e80-4659-b8f4-6efb6f6ca018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "40dac714-6b06-4166-908d-546b440b36b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 88
        },
        {
            "id": "040abac9-590c-4040-9e78-826a3c9ea2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 58,
            "second": 89
        },
        {
            "id": "56bf0f8c-3c45-4153-a51b-eb3d7a05d5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 59,
            "second": 63
        },
        {
            "id": "564e0f45-0c12-49fd-89c5-4067c25b8093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 65
        },
        {
            "id": "e5a46ca6-cea8-46d2-8365-d5bce991bb54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 59,
            "second": 72
        },
        {
            "id": "529da677-1d0e-4d80-8319-1e94ff5e11b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 59,
            "second": 73
        },
        {
            "id": "374945b2-9252-479d-bd3c-b16c282978aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 59,
            "second": 88
        },
        {
            "id": "2383ce21-6163-43fd-b912-28dc5ec196f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 59,
            "second": 89
        },
        {
            "id": "6eaffa0b-2f1d-490a-8145-f648e1727dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 44
        },
        {
            "id": "0f716cfe-b97d-4f92-891e-6282e4386cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 63,
            "second": 46
        },
        {
            "id": "3dd3ef3b-cab8-4853-937b-6f88a6dfff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 52
        },
        {
            "id": "ac94f71a-3375-4415-b9d7-1186892111cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 53
        },
        {
            "id": "dfcf8441-9bfd-474e-9a82-d5bc28d4b89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 65
        },
        {
            "id": "343612e8-c0c6-4220-a15d-4ce3dc6e2a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 63,
            "second": 74
        },
        {
            "id": "a96f73da-b9c1-42b7-80ea-04edd6293f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 83
        },
        {
            "id": "ff9b6201-d78e-49e8-85f6-0c51f8ca5273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 86
        },
        {
            "id": "73e05118-0e5b-4675-91e9-b43d767ada32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 88
        },
        {
            "id": "133bbcf7-15ed-4163-b143-aea4a5557ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 89
        },
        {
            "id": "5d4f0347-31da-4f39-826d-c9d22bbbe4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 34
        },
        {
            "id": "3d174138-0086-41d1-abeb-8e7df1412a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 39
        },
        {
            "id": "2b08f48e-148f-4227-95dd-cbf619308efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 49
        },
        {
            "id": "af5b2ab4-b447-4a9f-a851-54263665d2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 52
        },
        {
            "id": "b5cd1269-5e27-4203-b59c-573abb5c052d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 59
        },
        {
            "id": "eb24a52b-42b0-4c45-8aa5-0d7157f83379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 63
        },
        {
            "id": "f3aff825-7e81-4cd4-88b6-dbd98d8aadf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "81d152f6-b639-45b4-bc8e-5747e2c809b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 73
        },
        {
            "id": "001768de-1e16-46db-af2a-728d04788cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 74
        },
        {
            "id": "4aaf3919-14a2-4b54-a570-14c64ff2aaad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 76
        },
        {
            "id": "e484592d-dd35-4ca0-80dc-7c95790218a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 77
        },
        {
            "id": "d38ebf2e-d9ba-495c-9362-b5365ff02a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 78
        },
        {
            "id": "f0e2f1cd-8f81-4468-b784-7549c03c0f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "a9a60ce4-825a-4e74-bfb3-ae4c61af2856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "1f8d5987-caa3-44b4-b5a5-1657c37f41bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "ee0f225a-fbfc-4b84-a594-3a50d09d86e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "836bed31-c1f6-4991-a02b-79e360deb1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "32c50a4f-c696-4fb3-93a6-ca93eb87cc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "1285d15c-b956-4b73-9e2a-23090dd59a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 90
        },
        {
            "id": "fe324264-4d00-4ee3-ac78-770fb6c82861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 118
        },
        {
            "id": "7f52d0b1-fe4d-484e-bb47-0525a5866eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "b61521cd-b5fd-4232-b2ca-71a4eab66448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 121
        },
        {
            "id": "a514790c-3bd7-4de5-884e-5781b65908c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "d1063a30-df4b-4928-9b8e-4c38e41d5e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 67
        },
        {
            "id": "d5ba1185-d12c-47b1-91ce-2fc4e268d6f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "f336f62c-0ae5-4dc3-9e7b-23dbad39e83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 73
        },
        {
            "id": "38164c58-73b6-48c9-961d-1fe6316bedf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "1ac4a5fa-7fa6-4f28-bc22-5bd3665420de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "48212d8a-aea1-4052-a249-3f881de0d470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 86
        },
        {
            "id": "38492070-0780-4674-81fe-c2171d3307a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 87
        },
        {
            "id": "11a47513-82a5-4f2d-bbcd-95b9ca3bd2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 88
        },
        {
            "id": "a9d6d03d-93f1-4d63-8b7e-54be7eb41ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 89
        },
        {
            "id": "859ace4b-b01e-416b-8fd6-d8a7e50b3e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 34
        },
        {
            "id": "5c6fa434-9df4-4f00-b5fd-b5353f3ee9d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 39
        },
        {
            "id": "599fbba1-23cb-4024-9aeb-6b4167592445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 48
        },
        {
            "id": "a0be7ad6-5e1d-44d1-880c-bd0f200535f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "5ae80d8b-73ec-48d4-9975-70ac9d12963a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 52
        },
        {
            "id": "34539120-8fb7-4ec7-8cdb-657b81208b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 54
        },
        {
            "id": "d610d4ea-6bc7-4921-9508-9537b84f0df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "c4c573b5-8811-454a-a558-f866f08bbdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 63
        },
        {
            "id": "d78f29f9-f853-4531-a005-4904a92f8ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "2085ecf5-651e-42cb-a4bf-701932206ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "5bf4f6c0-e99c-465d-9779-317f77e274c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 74
        },
        {
            "id": "370fe371-a087-4733-ad3d-8261ca6b1244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 81
        },
        {
            "id": "375dacc5-9c6a-4d9a-be70-337ecc9b6a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 83
        },
        {
            "id": "1cdd1024-593e-4dcb-832b-67d01625f2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 34
        },
        {
            "id": "8616cc97-87dc-458c-809d-28f9dd87f393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 39
        },
        {
            "id": "de2386dc-b323-4980-ab00-11bb5b40ae2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "57b640e5-5963-463a-8c00-9fcf39137cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "3e0ee560-8473-4159-8efa-0076ac5140b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 51
        },
        {
            "id": "fa032cd5-4e3e-4ef4-a906-c3c02586b799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 58
        },
        {
            "id": "b67a4799-4a22-471c-9525-5723103f7f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 63
        },
        {
            "id": "75dca902-d906-44f2-9200-a8dd9d79e402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "40bb6fba-4984-4db5-8ca2-551610343254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 69
        },
        {
            "id": "bf1e4eb3-cfd6-4469-92da-b18a4e42cde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 72
        },
        {
            "id": "78383ee8-a56c-4912-a501-1729ae2ea9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 84
        },
        {
            "id": "aef089dd-06b7-4413-8097-101b8c181cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "25b9966b-824d-4b5f-8b62-cf353f86cdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 87
        },
        {
            "id": "d4c185ef-06ad-4c71-b866-93eb7df70f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 88
        },
        {
            "id": "3d833667-f4c1-4617-8dc8-fcea07f9021a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 89
        },
        {
            "id": "a3c5d4ce-8657-4795-9be6-c24068ada80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 70
        },
        {
            "id": "b4392f38-8d3f-4eee-a492-17a39698a410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 72
        },
        {
            "id": "eacd14dd-893a-46df-91ed-c17fd951014e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 73
        },
        {
            "id": "3e13f3e9-c528-4875-91e7-c774a597e02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "9889d54a-0066-43b7-97f9-ea73fc7074af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 90
        },
        {
            "id": "6b404d92-0b53-4098-a874-97df586f47c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "e7b34d48-c64c-4361-bff4-fe24def55e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "b303f392-eed2-4265-adca-a827115f5d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "58a20c48-756a-447c-9486-a812487f01d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "f01bb984-51ae-437e-ad14-d9522c79b5dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "76d7ecb4-fb70-4580-ab96-2d756286fcc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 111
        },
        {
            "id": "280129a9-ef1b-4200-ba2f-fba0ff47dd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 63
        },
        {
            "id": "0419d217-7665-495f-bc70-d767014e41f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "820df035-7359-488f-8643-2f6ac7ecc00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 73
        },
        {
            "id": "22d2e23b-4e47-4614-aa8d-3ca35181b6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "63eac437-3541-4b0e-869e-fae511377a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "711c32ec-7b27-4d00-9c51-a6a6dd7d2629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 67
        },
        {
            "id": "cb75410e-7df4-4737-b2bb-9fd5f859c4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 72
        },
        {
            "id": "b1fe5f36-daec-434a-aedb-04f3c2551e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 73
        },
        {
            "id": "56013774-a0b9-4398-b460-98076769ed74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "2a1aee53-d197-4de9-8f2f-6f126decb749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 44
        },
        {
            "id": "1f1db07f-6fd9-4c3a-be06-fb195aa0df17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 46
        },
        {
            "id": "e774b4cb-def3-46aa-9d37-5da8bfe9ebd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 65
        },
        {
            "id": "a2d282f4-ee96-4896-96ba-2bdb500634c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "a9b02226-996a-4629-8a3d-d2807abb70ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 88
        },
        {
            "id": "3590428e-dac4-4cde-89d5-dbc269d162f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "08a25fbd-d4af-4dc0-97c2-6558f6750f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "1be88ba2-35b0-443f-b8fc-3011922fe62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "b91f653b-4b40-4df6-a597-37e4b8aa26f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 53
        },
        {
            "id": "34317082-e4fb-409a-82bc-f5c616f74b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "a15b83c7-7bfb-42c2-bae6-12f24e3a9525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 74
        },
        {
            "id": "cd26880e-58a6-46bf-9e3d-2b782ff57b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 75
        },
        {
            "id": "1b634bdb-d389-41a7-ab5b-34c87f60eec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "1d1ac4e7-896f-45b4-b5ca-c240e96a4697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "85bb099c-b8ae-429b-a1ba-29ab314b35dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 44
        },
        {
            "id": "c144d7ee-6ceb-402c-99b2-96a1a68b26d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 46
        },
        {
            "id": "b9d3a4ba-e41d-4296-a5d4-3e502017a037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 48
        },
        {
            "id": "606dc39c-de9c-4253-9c23-6016d0865732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 49
        },
        {
            "id": "3b1b382a-e4e6-40b2-8f70-468de741a33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 51
        },
        {
            "id": "86156d73-a860-42d3-b9be-5f13983da96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 52
        },
        {
            "id": "9f022421-db94-4c09-a320-d4d567f6abed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "1ed0eeef-4a35-41a3-b1c8-7deaf1a5b8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 54
        },
        {
            "id": "cfa94109-1572-46eb-b379-d5fbeab5a44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "35d75c74-08ee-4f27-81f8-e39433e673c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "5d16b855-3928-49f1-b037-4a25cd7bd4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "1d7b9a5f-83d7-4b77-b555-0fbed71eff90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "34852ed7-4fce-42f3-89cc-84e4137fbfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 63
        },
        {
            "id": "f3cff50c-944f-4fe9-ba01-b66e2e8237eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "42ab1839-0c30-4d27-bc2d-cca1d5aee8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 71
        },
        {
            "id": "ce008b26-7174-436e-96f1-dbb13f932d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 74
        },
        {
            "id": "25282368-25b3-44c0-a80f-548e9c3c0f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "440189f2-e7a4-492d-a5ad-eb1eae4ab803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "530cbef6-6db8-4427-8b9c-9d404da2f12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 83
        },
        {
            "id": "f70988f4-7bc8-4037-a88c-9fe316cdafd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 111
        },
        {
            "id": "4ccf8e13-6791-4687-ba94-63ab40cdaf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 34
        },
        {
            "id": "514c7dc6-e93a-4b25-b6a8-8db4b8807bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 39
        },
        {
            "id": "2c45df05-1edd-4775-9a42-620ec11cdde7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 49
        },
        {
            "id": "77ecbd40-635c-4551-92ef-17f2dac02351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 52
        },
        {
            "id": "78004639-f1f0-4cdf-8f28-0b992914595b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "13cfc027-34b3-4678-a4ff-cbb5ddc2f762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 63
        },
        {
            "id": "0df42d4a-a362-4775-a75b-dfb87a8614fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 73
        },
        {
            "id": "5cda41a7-839a-47b3-85f0-0a83060d4627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "88eba3e2-18da-4586-943f-952cb1882633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "8c0055fe-1468-4ee5-92b3-9314a34118ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "ceb362b2-cca1-49f3-aa4d-c1188234af52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "b35cf051-df82-4458-bfae-c44dcf71cf0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "5cc396d8-e3c4-4914-a421-dae43b0ff3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 69
        },
        {
            "id": "e0f91382-72d6-43ef-a7a8-9dcb229dcc1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 78
        },
        {
            "id": "fa2ce9fe-46eb-4fe6-a2bd-115f974e02d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "aaa8a918-e3f3-4776-8b9b-20926167d1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 34
        },
        {
            "id": "51f73750-4f09-48e9-b0f1-01310c58dda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 39
        },
        {
            "id": "002b9dfc-20e0-4543-9fcf-259833abf78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "247a7087-6314-41b9-8291-7714d6ae3b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 55
        },
        {
            "id": "43bc0291-66ae-4151-8639-43f4d823a983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 58
        },
        {
            "id": "709eea07-2814-437e-80df-1b35cb71aa96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 63
        },
        {
            "id": "2c8bf5bb-45cc-4c1f-9e23-1eb45b8c6e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "68f9824f-6d52-47bd-954f-76dbd081947d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 67
        },
        {
            "id": "f212b559-1e4d-45c5-b958-839e6cd8e3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 72
        },
        {
            "id": "a1812595-d746-44a0-8872-17ef759cb852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 73
        },
        {
            "id": "2d3e64bb-6763-4a77-9f5d-4c8e4fea9ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 78
        },
        {
            "id": "0a89c4e0-bd51-4d95-a7ed-998cce8fed80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 84
        },
        {
            "id": "c8e22112-cdce-49e3-9235-6f49d34a3311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 85
        },
        {
            "id": "e6d5f7f3-5740-4281-ac32-75bf9ec1f76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 86
        },
        {
            "id": "a0c6c462-0b95-4654-aac0-41b93ad57b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 87
        },
        {
            "id": "607ff3b4-dc25-4725-b5ca-e5a94c59c5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 88
        },
        {
            "id": "fd021928-7a9d-4be5-83a2-93d409962fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 89
        },
        {
            "id": "21214879-f5c7-4db0-932b-6d2c1a9f4921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "9c43a1d4-91a7-4563-a17e-dfcff05794e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "e3d0a02f-f7e4-44f0-8860-62add3d5c892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 53
        },
        {
            "id": "a49875d6-b1bd-445a-bd63-c15f2fb1b03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "1a939c45-ce48-4616-a6ed-6351a2d910d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 72
        },
        {
            "id": "e34a6d88-6379-46d1-ae1b-a98068625cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 74
        },
        {
            "id": "feb19520-9778-415d-8f7b-dcf6f236eafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 83
        },
        {
            "id": "23d54483-dbaa-4a8f-bd39-a834a3e9c12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 86
        },
        {
            "id": "83332199-d334-4562-9694-f62989aba747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 88
        },
        {
            "id": "45af6f45-8b2e-44e8-a593-2789cd8bf9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 89
        },
        {
            "id": "fc586e24-fddf-4af8-a7cb-eee414e04002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "3e69b9b3-0a61-4f41-980f-139436186543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 115
        },
        {
            "id": "677b8932-c4b9-42d1-b5b2-cce4099eaa36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "d77fe2b4-da9d-4ccb-816b-af8586102b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 34
        },
        {
            "id": "05131baa-3fc8-40f2-9af7-1dd072f928f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 39
        },
        {
            "id": "167f005b-4aa1-4f8a-873b-68232e8c4a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "a2c5e735-cf8d-47fe-943f-4ca808ba36eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "4d0a70c4-e908-49f9-ad94-eb517c41e2f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 48
        },
        {
            "id": "99481adb-4b45-4926-a7fe-a515ab6fbce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 49
        },
        {
            "id": "4e4bd3f3-fcb7-476b-a91b-bc2d80f45a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 51
        },
        {
            "id": "e307c8a6-3efa-402d-9fc5-4bce9cc40d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 52
        },
        {
            "id": "4c69ad86-7c62-41ba-8ebd-6b47f3eeff3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "ee0b4ec7-a827-4c10-84b5-5d9f24224bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 54
        },
        {
            "id": "dcc7051f-eca0-4b83-8dab-e1d3b41ba288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "0d3a770a-9d66-4b14-a261-490b1fe69eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 56
        },
        {
            "id": "89fb2f43-c77d-4fb1-8293-2c9fd01accfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 57
        },
        {
            "id": "beeea090-3ac9-46a8-bb8b-8aeaa4a7a1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 58
        },
        {
            "id": "2c0e7810-c4cb-4b26-bbe2-6acb690b7f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "7ecc3953-b7f9-4c5b-9f9f-7f2c0513d494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 63
        },
        {
            "id": "22ac25ea-0a14-4c55-a927-3b85ec733daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 67
        },
        {
            "id": "f3069014-81c1-40a7-a86e-0dacac45e97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 71
        },
        {
            "id": "940e03a4-06c9-4fd5-8d24-901ba9e71d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 74
        },
        {
            "id": "5eaa1e5d-de15-4d14-8f0c-fd39c32b4466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 79
        },
        {
            "id": "118fbd3a-e724-4c27-8079-f7ad14513bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 81
        },
        {
            "id": "4236c7bc-8196-4e1d-8cc0-06056c43db1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 83
        },
        {
            "id": "29576c27-acef-4987-bc77-0c0502c41d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 84
        },
        {
            "id": "ba0b4daa-b01a-454c-a021-22d9844c4bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 85
        },
        {
            "id": "395be970-e52a-40ab-bbbb-67e210f7d9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 86
        },
        {
            "id": "0a2621f2-a5b8-43e6-bf77-43c494a2cb21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 87
        },
        {
            "id": "28dccfd6-0006-43df-ac42-18b70701399f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 89
        },
        {
            "id": "5b6589d8-8c79-46a8-b54a-a19fe370d116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "c5d5914f-66b5-4dd1-b593-fe1aecab3970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 44
        },
        {
            "id": "04e40c78-2f1f-479a-b5cc-d5bfe10e7eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 50
        },
        {
            "id": "16ca32ec-d748-4df2-b00e-07204520a4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 52
        },
        {
            "id": "595322ef-1ae9-4229-800d-50b173a6cb0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 65
        },
        {
            "id": "2f2875dd-7591-4110-a603-34d25a265874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 66
        },
        {
            "id": "27faab14-6f19-457c-9679-911d6a1cd9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 68
        },
        {
            "id": "b1aa6f89-4852-401d-b053-95921c8c4ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 70
        },
        {
            "id": "6308f74d-52f4-4de1-bde6-2b4699274d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "efe91115-ea55-4012-9a51-d39cbbefe572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 72
        },
        {
            "id": "9ec0e018-fffe-41d9-875d-51550fa55305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 73
        },
        {
            "id": "a543a48f-c61a-402e-9844-9b4bb77f8d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 74
        },
        {
            "id": "9be8643a-2f16-4b5c-80b6-799e76c27e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 75
        },
        {
            "id": "c6e5ff7b-25a6-489e-9ff7-ceb12c20c3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 76
        },
        {
            "id": "a1bd318d-5238-4eb4-b12a-a1ae586328f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 77
        },
        {
            "id": "771e3e29-8202-40af-9450-bad0cc0560c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 78
        },
        {
            "id": "270f85b4-f481-478c-8cbb-e4714690f6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 80
        },
        {
            "id": "a91c5a8e-d34b-41b3-89c4-a115ad3356e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 82
        },
        {
            "id": "61fbb7cd-55b9-4ff6-a8af-9b89fa006d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 83
        },
        {
            "id": "f760d792-6e0c-4792-98c2-01aebde4b13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "22053d9e-69fa-4fa9-9eb2-049c600762dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 86
        },
        {
            "id": "8eb06760-fad2-484e-b008-e4af0ad63746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 88
        },
        {
            "id": "e6bf8f4c-53a1-436e-a970-2e294c580882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "4fe0b6ab-4635-4626-8bef-ffd09b600d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 90
        },
        {
            "id": "afc22e13-8968-4b02-a652-c8c49cbfed76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 49
        },
        {
            "id": "64c053f9-73f9-464a-ac05-b4ebb79bc49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "3581a8bf-3867-4488-86fd-9ea3cd6b7d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "f2b683ee-659c-4e59-9924-87377429f7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 72
        },
        {
            "id": "12124512-09ab-4b01-97f4-d57b809e6ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "1f3cdef9-ac24-4549-b7a8-940ed0f5c1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 84
        },
        {
            "id": "a0c1996b-1de7-4a48-a0b9-46b7f9df648f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 88
        },
        {
            "id": "59e7424d-f06d-4ae7-9c9a-054e0991304c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 89
        },
        {
            "id": "872ab608-e466-4d5b-ad05-f61282da305c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 34
        },
        {
            "id": "18ace5d1-799b-42d5-842d-ece5b0c71b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 39
        },
        {
            "id": "8f9c101f-db9b-4054-be0e-cdf977d4188c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "5fdaac1e-0d2b-44e4-aef9-c5e6faff8479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "6f541b5a-a9b8-41d1-9bce-af20c71f2b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 52
        },
        {
            "id": "9ee40946-7459-4f9b-b1da-b20698689df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 53
        },
        {
            "id": "059c80ef-deab-47ec-a6b3-f7e4672bd129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 59
        },
        {
            "id": "1c9e2ec6-99cd-4e4c-ac5e-0b64036cce0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "e135efe6-55fa-48df-81bf-8838e1be2a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 74
        },
        {
            "id": "d1c503cf-8c13-4a9b-bc9c-078d715f3dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 83
        },
        {
            "id": "b12d959c-e937-4b71-9ff8-35adaf269eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 88
        },
        {
            "id": "b1a17c12-564d-4bfc-8228-2f946737a3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "ce1e8d11-2ec9-453c-9e21-e095b006d4a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 114
        },
        {
            "id": "6025c068-fd1c-446a-8b1b-8f9e2c790329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "d391fe96-a551-4c1c-9bfd-bb83487daef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "a6256fad-d162-4fd3-80e6-0d3dd3952d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "5898a9c7-84c2-4486-91ae-fb94d425469b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "dc84fb85-7759-4f11-95c5-07e6711e9afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "e52e8738-29db-410b-9e51-742dc4c7fdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "d6fde28b-d94a-4f9d-9cf7-4c53bd54fbd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "d8a5a69e-226a-4376-9960-77b624ba4028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 39
        },
        {
            "id": "d57e85c3-01ab-4eb2-a952-734b69497b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "e5678e0a-9c1d-4a1c-8f0d-b52b58a341a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "f842f66b-82a9-4488-b9d8-c2cd3c7e8e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 48
        },
        {
            "id": "2f6d981c-83fe-48d9-b2c7-6118d63bbefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "1d34f059-931b-4117-8593-73cbc5c8c48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "8c7a9ba3-228d-4a3e-8a15-85b95b9cfdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "a222eabb-af7f-45a0-9c5b-a0bd9b4559cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 52
        },
        {
            "id": "a20a0423-d058-405e-b6dc-b733d1d1512e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 53
        },
        {
            "id": "36f8e86d-d4ca-4ea6-9465-376c127f7347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 54
        },
        {
            "id": "dfac283a-7b41-462a-b179-bddb872e9635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "59ebb8bb-51b3-45ab-a27e-6c7cf6c87022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "c1c8e833-f460-4836-b6a6-abccabff41c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "71a9343c-bac6-412f-9da3-90f07175a0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "549aa080-a2ab-435a-834b-31c6a6181eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "313e96c0-77e4-42c0-8b86-d10311a900cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "3b16e1fa-ffd2-49fc-b328-a6b235d53b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 74
        },
        {
            "id": "67921a07-7892-44a8-92e9-41fea1b36ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "603ad658-a90b-4262-9bff-e317cda717fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "be8b5c15-a9ab-4fdc-bd2f-92c207e8defd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 83
        },
        {
            "id": "9e1f1b37-d02d-4638-981d-1cc7b02c6b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 88
        },
        {
            "id": "d4bede1d-ddac-4895-9aa5-5425f24b1f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "0ae2cc4f-a714-41cb-a7a7-af45aa3b52c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 114
        },
        {
            "id": "b8cda439-345a-4120-834d-efed270569a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "de55355a-1187-4c6c-a793-e1d957b77a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 39
        },
        {
            "id": "7b32e7e8-2040-4b19-a039-8422b8e95bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "c063f784-0bad-45af-9409-88c33c8561d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "6251f46f-17b2-4065-a197-8e2407fa64e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 48
        },
        {
            "id": "f732de72-708d-4044-9fad-f1fdd1d6bf68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 49
        },
        {
            "id": "15d62866-9608-485a-a435-f0c0afb5b15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "364a5b75-0260-4022-a13d-ab44a1c224fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "967e3069-04ff-4df0-a7e2-22bf181a8139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 52
        },
        {
            "id": "44fce611-4496-4155-9749-620408780318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 53
        },
        {
            "id": "698d50f8-5b64-4adc-aea6-66a3a0fab75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 54
        },
        {
            "id": "39c38bd4-9b92-4155-8aa0-ad83ef9a221e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "9c0d9ac0-11f8-4293-ae7d-09461405a4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 57
        },
        {
            "id": "986e76d5-353b-45bf-836b-7966c477bf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "9a98ba83-175a-43fd-9290-b93325bdd228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "428ffa44-7f0e-47e8-a07f-f1a14bf681aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "951d8cfe-f398-47af-ae0b-3036a8ac5992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "ef0d6112-e4d4-4944-8cc6-c32342cde25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 74
        },
        {
            "id": "ff67aa29-a1c1-49f0-9b22-dedde9b47215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 81
        },
        {
            "id": "7b0d592a-befe-4458-b8c7-7219fdd99897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 83
        },
        {
            "id": "69302408-24de-4353-a556-717045354390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "51f0fa81-9eec-4f9c-a4e0-926c4c03b8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 114
        },
        {
            "id": "71d5507c-84a4-43a5-a1b5-ab7388bab621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 46
        },
        {
            "id": "ec94fb08-ecb5-423a-80dc-3fcc0f411149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "cdd999e3-ad28-49f7-8bce-5b400e75f7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 49
        },
        {
            "id": "413fdffb-c2b3-4ed8-918a-b5606f75a6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 51
        },
        {
            "id": "740d78ae-801a-471a-8a59-106b3f352bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 52
        },
        {
            "id": "7ed84ede-8060-4e0d-8e59-de0cc2761baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "f9579fbf-622c-403b-acba-5d0eccfd5762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "fc8501c3-c575-4604-99d8-bae3baaeeae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "350cd985-95d6-463d-b002-53b1db9725e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "99d1f8e7-2a94-439a-bb91-8f9fd8701cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 58
        },
        {
            "id": "40c85f1c-f0e1-4cd0-bbd1-968aff9a4ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "3f5b6e07-311f-4c38-b2c3-42777dfb92e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 63
        },
        {
            "id": "0f14f648-6416-498a-8057-98b99f2212e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "5750bb90-9cc3-4063-a60b-7772d40e96e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 71
        },
        {
            "id": "a8914738-f731-429b-b6ef-4915e250f76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 73
        },
        {
            "id": "2cc1fc92-585d-4e3f-8e75-5718b436e1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 74
        },
        {
            "id": "6bfca465-72dc-4073-81e8-011e9a0be368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "7517af28-126c-4569-bf78-f01d33d5adbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "533088f8-6d66-45a6-847f-0ca679608cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 83
        },
        {
            "id": "7ae2eee9-c068-4023-9f27-8c76f820bf17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 89
        },
        {
            "id": "a630f9c7-b7a1-4526-9962-52dd32a7dfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "5170634a-631c-4c44-bab4-b6ad1d2ed9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 46
        },
        {
            "id": "69a4a3e3-b346-43be-82b4-8c4daef10a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 48
        },
        {
            "id": "ab896b75-0186-43f7-9f34-9ec96e2ce807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 49
        },
        {
            "id": "6d964748-27a5-4eea-a0e7-af122e5ed376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 50
        },
        {
            "id": "f651b954-a8c2-4780-a523-079cf344b416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 51
        },
        {
            "id": "475eca00-183e-4b9a-b6b9-207608cb79e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 52
        },
        {
            "id": "e09e5bd2-99e5-4482-aa26-c77f58e143a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 53
        },
        {
            "id": "4e84140a-1a4a-45b9-9bb4-67fc5cea39cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 54
        },
        {
            "id": "cc43890b-e06c-44e8-a15c-b6766a448918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 56
        },
        {
            "id": "f4dc4bc2-cdcd-44e1-8842-3fe05ab09ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "96803cc0-803f-4684-a926-6e0a0aab9b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "6477b01d-88e4-4a51-a3a7-491ac7e866d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 63
        },
        {
            "id": "95ba6dea-f45c-4ae5-a1fd-a6e914ebfc94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "c15ec549-67be-44b0-885d-36e66ab0046a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 67
        },
        {
            "id": "8c39c236-3e44-410c-818e-8a0655c48ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 71
        },
        {
            "id": "adfdb8e9-485c-4962-b09e-c5f67c199a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 74
        },
        {
            "id": "9fcdddcc-9d01-4428-a2ff-9a479055a586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 79
        },
        {
            "id": "291ec412-2ec1-4cc9-8c4b-4a6956a397ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "80f2a553-6e24-47b1-925f-5cd6292ee2f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 83
        },
        {
            "id": "01fa8a45-8bd7-4b82-8ee5-34b610627abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "5a51711f-2bd6-40b6-aaa9-839a50ebd3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 89
        },
        {
            "id": "1bc17b66-0b49-4b35-bdd3-7051a2ee1fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "f32e28b3-8c01-4e91-ad90-684c7f828068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "30973b52-05bc-4d13-843a-2fd179d31899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 44
        },
        {
            "id": "b115183b-7a34-4278-8b60-f444de13d560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 46
        },
        {
            "id": "67dba7d2-5fd1-46ac-8e40-1efcd9c84454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 52
        },
        {
            "id": "736d7f6a-8836-4d7d-9dec-2223cabf212a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 53
        },
        {
            "id": "1f8a4f85-4c20-467e-96e9-555ca13c5a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 65
        },
        {
            "id": "55a23d03-7d47-4ac2-b7eb-bba26278298a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "0c92bfdc-778d-492f-b58b-af63706d3a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 74
        },
        {
            "id": "e126f3f9-e968-4148-a887-a43470fcfab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 83
        },
        {
            "id": "68761dc6-d5e7-4a6d-9d41-11f81ee722c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 88
        },
        {
            "id": "4dca0681-81f1-456c-9780-b8496245cb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "dafff587-9fa7-48ce-822e-06587b553e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 99
        },
        {
            "id": "6458a6fe-8a56-4445-8d8a-5ded9030ff8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 101
        },
        {
            "id": "5f689ec4-a2b7-418c-9444-10253a679ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 102
        },
        {
            "id": "57d1e456-736f-4daa-bea0-e3f44ba29348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "dbdda258-7652-4d3d-8157-20acb8e4db93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "545bd8db-8dad-4908-b90f-252e8fdd9b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "5880baa1-7096-4190-9f0b-9b3c99962607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "f7a45fc8-71f9-41ca-9eda-81a945c83299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 107
        },
        {
            "id": "80e87886-0dfd-4e5c-83cb-cc440967291d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "1c67eacd-dfc3-4364-a2eb-08686b339192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "04dd9dbe-956f-415c-8571-fb952cb2f232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 111
        },
        {
            "id": "c147f451-8ff8-44fb-8f6e-54040ffa4278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "6cc15616-8bcb-4efd-b777-71696626f4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 100
        },
        {
            "id": "bb88bf40-e736-4f84-a328-7c9cafd371d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "1219845e-30ae-4aaf-a6bf-1cefc0a70d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 115
        },
        {
            "id": "71ff904c-f50e-4706-85f7-92fa7f85ce20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 116
        },
        {
            "id": "f933fa7a-53d8-4125-acce-13ff9a14b9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "ab788cc7-7913-4ba4-beb2-e37c8ab48777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "9a7b7ba7-3b1c-4732-bced-100e01a2958c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "7aa01e58-5d38-491d-9912-94d2d3582584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "ec0462d3-2b2a-4dc6-b285-d2230fabb366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "2d802bc2-a23e-4663-b006-47178ef550d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 44
        },
        {
            "id": "13ea37cb-c95c-40b2-bfe0-c0a1c4d8a88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Kooky",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}