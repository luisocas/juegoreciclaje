sprite_index = spr_txt_base;
image_speed = 0.1;
image_index = 0;

///Variables de Texto
numero = 0;
texto[0] = "Hola Amigo bienvenido a RECIJUEGOS, \npara poder moverte en el juego solo debes \nusar las teclas direccionales";
texto[1] = "el objetivo del juego es recoger \nel material que se podra reciclar, evitando \ncolisionar con los objetos no reciclables.";
texto[2] = "Mucha Suerte en tu recorrido, Presiona 'A' \npara empezar el juego, tambien puedes \npulsar 'ESC' para guardar la partida.";