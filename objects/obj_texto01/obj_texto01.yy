{
    "id": "cc47908b-1082-44c0-98f5-24e33e9c0beb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_texto01",
    "eventList": [
        {
            "id": "12843c33-6a54-4131-b734-cafc9550143d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc47908b-1082-44c0-98f5-24e33e9c0beb"
        },
        {
            "id": "37f26dc7-4511-4443-a1a0-2378e133a6db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cc47908b-1082-44c0-98f5-24e33e9c0beb"
        },
        {
            "id": "fa8f2971-68fe-427d-b103-72e4b3b68dd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 9,
            "m_owner": "cc47908b-1082-44c0-98f5-24e33e9c0beb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "008755fd-411f-441a-b8d3-ba4606b252a1",
    "visible": true
}