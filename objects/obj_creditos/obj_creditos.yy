{
    "id": "8e868928-5ed6-43f7-b80b-a98265046401",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creditos",
    "eventList": [
        {
            "id": "6affc880-6e59-4df5-9c24-8539a615c6f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e868928-5ed6-43f7-b80b-a98265046401"
        },
        {
            "id": "6a81cbb2-fb63-453b-88c9-1db0a93c864f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "88ae2a1e-9415-40ec-8c42-4441aa3b1990",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8e868928-5ed6-43f7-b80b-a98265046401"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "abc547ee-e76c-47f3-acdc-1cebfcf11d27",
    "visible": true
}