{
    "id": "0f5cacad-2d1f-4f11-a42a-4f0151dc78ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_vida",
    "eventList": [
        {
            "id": "ee783544-9ee5-46d1-9987-fc0759a77b50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f5cacad-2d1f-4f11-a42a-4f0151dc78ba"
        },
        {
            "id": "b01e169a-32de-4b3d-9917-019f54af7953",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0f5cacad-2d1f-4f11-a42a-4f0151dc78ba"
        },
        {
            "id": "24ab4762-d49e-4a43-8c9f-6be0d1719bb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0f5cacad-2d1f-4f11-a42a-4f0151dc78ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4978955-c51a-45ee-a332-4ac96a2f1bf1",
    "visible": true
}