/// @desc Barra de vida
draw_sprite(spr_vida,3,x,y);  //Escoge la imagen y posicion

//Abreviacion
var estreche;
estreche = global.Salud/limite*250;

//Barra Verde
draw_sprite_stretched(spr_vida,0,x,y,estreche,46)

//Barra Amarilla
if global.Salud <=50
{
	draw_sprite_stretched(spr_vida,1,x,y,estreche,46);
}

//Barra Roja
if global.Salud <=30 
{
	draw_sprite_stretched(spr_vida,2,x,y,estreche,46);
}

//Diseño
//draw_sprite(spr_vida,3,x,y); Si hubiera Diseño de una barra

//Texto
draw_set_color(c_white);
draw_text(x+110,y+12,global.Salud);
draw_set_font(fnt_barra);