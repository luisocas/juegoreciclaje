{
    "id": "bc7352ff-1153-4c12-90bd-9e3422228e19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "5c634e7f-abb2-4dd6-ba16-a8fdb9e78775",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "c1441423-5a8a-4267-a38e-6109c5a47cae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "2e521808-8e6b-477e-9538-506f594ca756",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "b9b2d0d5-bd5c-4e8b-b997-e49e94ee3c79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "14e39f7a-e6c8-4214-b6f9-6f60eda838b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "2d1ea349-12b8-4a93-a9fd-2013430471d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        },
        {
            "id": "3dbfdc50-448a-4a37-a8a5-42f8a853c3b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "bc7352ff-1153-4c12-90bd-9e3422228e19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}