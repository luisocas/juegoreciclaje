/// @desc Dibujar puntaje y vidas
if room == rm_menu exit;
if room == rm_creditos exit;

#region //Dibujar Score
draw_set_color(c_red);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(fnt_texto);
draw_text(700,60, "Puntaje: "+string(global.Puntos));
#endregion

#region //Dibujar Vida
draw_set_color(c_maroon);
draw_text((view_camera[0])+25,22,"Vidas: ");
for(var i=0; i< global.Vidas;i +=1)
	{
		draw_sprite_stretched(spr_player_right,2,(view_camera[0]+25)+string_width("Vidas: ")+(i*33),16,32,32);
	}
#endregion
