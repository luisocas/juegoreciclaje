///@desc Controlar el nivel

/*#region //Activar enemigos y jugador
	if (instance_destroy(obj_texto01))
	{
		instance_activate_object(obj_player);
		instance_activate_object(obj_enemy);
		instance_activate_object(obj_papel);
	}
#endregion
*/
 #region //Vida del jugador
 if global.Salud <= 0
 {
	
	with(obj_enemy)
	{
		instance_destroy();
	};
	
	with(obj_papel)
	{
		instance_destroy();
	};
	
 };
 #endregion
 
 #region //Destruir todo cuando muera
 if global.Vidas == 1
 {
	alarm[2] = room*2;
 }
#endregion