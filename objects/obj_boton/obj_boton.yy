{
    "id": "7ad8d2cc-3564-4811-959e-e0919f44da57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boton",
    "eventList": [
        {
            "id": "d7c7e60c-4fb1-420b-99ae-8df265710f76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "1380998e-79fa-4a4f-9f32-5c6b6c1df6ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "b6064696-0f82-45c4-817a-69f899c650b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "ff729339-af7c-4699-bf70-e4e14e4ab9f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "e8ad7aa0-9038-4c0f-ad69-0fb67a78b282",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "74c64f29-8211-4a33-b6cc-3a380875e02a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        },
        {
            "id": "7fa2f1e9-2f55-40a5-b4b2-b88da774a300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7ad8d2cc-3564-4811-959e-e0919f44da57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ac9dac6b-ea31-4913-9d15-182908f127ec",
    "visible": true
}