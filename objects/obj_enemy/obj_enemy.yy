{
    "id": "0d29002a-c961-437c-b4f1-33d5a0cda811",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "1135e50c-f89b-4769-bc81-72aa1052f5d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d29002a-c961-437c-b4f1-33d5a0cda811"
        },
        {
            "id": "4ebdde10-6823-472e-ba99-f58c91f1f6dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "39902765-c6b2-42d1-ae6b-4d57cb23e878",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0d29002a-c961-437c-b4f1-33d5a0cda811"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8cca7e0e-d955-4865-88fd-a074c11d791a",
    "visible": true
}