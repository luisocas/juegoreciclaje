/// @desc Movimientos del jugador

#region //Gravedad
if place_free(x,y+1)
{
	gravity = 1; //mayor numero mas fuerza
}
else
{
	gravity = 0;
}

if vspeed >= 20 {vspeed = 20;} //limitar velocidad horizontal
#endregion

#region //Movimientos y saltos
var Der = keyboard_check(vk_right);
var Izq = keyboard_check(vk_left);

if Der {Izq = false;}
if Izq {Der = false;}

var S = keyboard_check_pressed(vk_space); //ord("A")
var A = keyboard_check_pressed(vk_shift);

if S && !place_free(x,y+1)  //&si no hay espacio
{
	sprite_index = spr_player_jump_right;
	vspeed = -Salto;
} 

if A
{
	image_speed = 0;
	sprite_index = spr_player_crouch;
	image_index = 0;
}

if Der && place_free(x+V,y)
{
	x += V; //Moverlo
	
	if Fall
		sprite_index = spr_player_fall_right;
	else
	if Up
		sprite_index = spr_player_jump_right;
	if Down
		sprite_index = spr_player_crouch;
	else
		sprite_index = spr_player_right;

	image_speed = 1;
	image_xscale = 1; //invierte el sprite
}

if Izq && place_free(x-V,y)
{
	x -= V;
	
	if Fall
		sprite_index = spr_player_fall_right;
	else
	if Up
		sprite_index = spr_player_jump_right;
	if Down
		sprite_index = spr_player_crouch;
	else
		sprite_index = spr_player_right;

	image_speed = 1;
	image_xscale = -1;
}

if Der && Izq
{
	image_speed = 0;
	sprite_index = spr_player_stand;
	image_index = 0;
}



#endregion
 
 #region //Verificar saltando o cayendo
 if vspeed > 0
 {
	Fall = true;
 }else
 {
	Fall = false;
 }
 
 if vspeed <0
 {
	Up =true;
 }else
 {
	Up = false;
 }
 #endregion
 
 #region //Verificar imagen
 if !keyboard_key
 {
	if Fall == true
		sprite_index = spr_player_fall_right;
	else 
	if Up == true
		sprite_index = spr_player_jump_right;
	if Down == true
		sprite_index = spr_player_crouch;
	else
		sprite_index = spr_player_stand;
	
	image_speed = 0;
	image_index = 0;
 }
 #endregion
 
#region //Vida del jugador
 if global.Salud <= 0
 {
	alarm[3] = room_speed*2;
	sprite_index = spr_player_dead;
	image_speed = 0;
	global.Vidas -= 1;
	
 };
 #endregion

 