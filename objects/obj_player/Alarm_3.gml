/// @desc Reiniciar posicion

if sprite_index == spr_player_dead
{
	direction = 0;
	image_speed = 0;
	x = xstart;
	y = ystart;
	sprite_index = spr_player_stand;
	image_index = 0;
	global.Salud = 100;
}
