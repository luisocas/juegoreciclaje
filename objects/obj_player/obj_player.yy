{
    "id": "43cb970a-ac95-4802-a4bf-9e619e3707bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "be0c577b-c333-4753-b5ae-80b222a98b21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "136b3b6e-60dd-435f-a590-4143dd31284d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "c6b119d8-8df2-429a-addb-928644d7a3ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "88ae2a1e-9415-40ec-8c42-4441aa3b1990",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "8fa578ce-c429-4d80-b172-346e862710b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "d42f18d0-63e2-46ea-a60c-6bb89576bc55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "54c2491f-3d33-4e31-a904-29cef217ed97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 67,
            "eventtype": 9,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "abe8b6b1-52e0-49af-9308-80c2c09c92b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0d29002a-c961-437c-b4f1-33d5a0cda811",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "73d961d4-7f48-4e3f-9be8-c7101916d472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "5a81e498-85f5-4f8a-aa58-a1c386290cec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d34f009c-676e-463a-81b4-ee3bf8275355",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "29cfee67-91a1-4e82-a392-dcf6580d3933",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        },
        {
            "id": "dc7497db-2df7-4908-b423-144a164cfdf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "43cb970a-ac95-4802-a4bf-9e619e3707bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ecbd44ff-b7d1-450b-9d84-fde8b93ed972",
    "visible": true
}