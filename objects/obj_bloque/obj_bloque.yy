{
    "id": "39902765-c6b2-42d1-ae6b-4d57cb23e878",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bloque",
    "eventList": [
        {
            "id": "6672e459-77b4-44ca-9d4b-c8378fdf7b6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39902765-c6b2-42d1-ae6b-4d57cb23e878"
        },
        {
            "id": "e3535ea9-5438-44a0-bf0a-bd89a71894b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "39902765-c6b2-42d1-ae6b-4d57cb23e878"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad215019-2f32-4ad8-b114-51e223df5204",
    "visible": true
}