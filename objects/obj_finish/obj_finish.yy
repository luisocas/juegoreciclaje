{
    "id": "64ba5b91-7b19-4d54-933b-52fa6af39957",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finish",
    "eventList": [
        {
            "id": "1f1501ea-07b9-4181-8a7e-5103c8dd3b6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "43cb970a-ac95-4802-a4bf-9e619e3707bd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64ba5b91-7b19-4d54-933b-52fa6af39957"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d8e7473-6a80-4414-af89-020b5e5eb4e2",
    "visible": true
}